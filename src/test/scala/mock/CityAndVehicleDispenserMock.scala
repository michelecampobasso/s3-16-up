package mock

import java.util

import akka.actor.{ActorRef, ActorSystem, Props}
import entities.utility.Logger
import entities.{Lane, Semaphore}
import messages.bind.{BindLaneMessage, BindLoggerMessage, BindSemaphoreMessage}
import messages.init.{InitLaneMessage, InitLoggerMessage}
import messages.utility.PrintLogMessage
import utilities.enumerations.{Direction, Flow, LoggerPriority}
import utilities.{CityMap, NextPossibleLanes}

/**
  * Mock of City and Vehicle Dispenser for testing some behaviours of Vehicle
  *
  * @author Michele Campobasso
  */
class CityAndVehicleDispenserMock {

    private val nSemaphores: Int = 10
    private var system: ActorSystem = _
    private var logger: ActorRef = _
    private val semaphores: util.ArrayList[util.ArrayList[ActorRef]] = new util.ArrayList[util.ArrayList[ActorRef]]()
    private val lanes: util.ArrayList[util.ArrayList[util.Map[Direction, ActorRef]]] =
        new util.ArrayList[util.ArrayList[util.Map[Direction, ActorRef]]]()
    private val LANE_CAPACITY = 25
    private val randomGenerator = scala.util.Random

    def actorSystemInit(sys: ActorSystem): Unit = {
        system = sys
        logger = system.actorOf(Props[Logger])
        logger ! new InitLoggerMessage(LoggerPriority.LOW)
    }

    def semaphoreInit: util.ArrayList[util.ArrayList[ActorRef]] = {
        for (i <- 0 until nSemaphores) {
            val row: util.ArrayList[ActorRef] = new util.ArrayList[ActorRef]()
            for (j <- 0 until nSemaphores) {
                val sem: ActorRef = system.actorOf(Props[Semaphore], "Semaphore" + i + "-" + j)
                sem ! new BindLoggerMessage(logger)
                row.add(sem)
            }
            semaphores.add(row)
        }
        semaphores
    }

    def lanesInit: util.ArrayList[util.ArrayList[util.Map[Direction, ActorRef]]] = {
        for (i <- 0 until nSemaphores) {
            val row: util.ArrayList[util.Map[Direction, ActorRef]] = new util.ArrayList[util.Map[Direction, ActorRef]]()
            for (j <- 0 until nSemaphores) {
                val street: util.Map[Direction, ActorRef] = new util.HashMap[Direction, ActorRef]
                if (i > 0)
                    street.put(Direction.NORTH, createLane(Direction.NORTH, (i, j), (i - 1, j), LANE_CAPACITY))
                if (j < nSemaphores - 1)
                    street.put(Direction.EAST, createLane(Direction.EAST, (i, j), (i, j + 1), LANE_CAPACITY))
                if (i < nSemaphores - 1)
                    street.put(Direction.SOUTH, createLane(Direction.SOUTH, (i, j), (i + 1, j), LANE_CAPACITY))
                if (j > 0)
                    street.put(Direction.WEST, createLane(Direction.WEST, (i, j), (i, j - 1), LANE_CAPACITY))
                row.add(street)
            }
            lanes.add(row)
        }
        lanes
    }

    private def createLane(direction: Direction, start: (Int, Int), end: (Int, Int), capacity: Int): ActorRef = {
        val lane: ActorRef =
            system.actorOf(Props[Lane], "Lane:" + start._1 + "-" + start._2 + "to" + end._1 + "-" + end._2)
        lane ! new InitLaneMessage(semaphores.get(start._1).get(start._2), semaphores.get(end._1).get(end._2),
            LANE_CAPACITY)
        lane ! new BindLoggerMessage(logger)
        logger ! new PrintLogMessage("Mock City and Dispenser", lane.path.name + " created", LoggerPriority.DISABLED)
        semaphores.get(start._1).get(start._2) ! new BindLaneMessage(lane, direction, Flow.OUTGOING)
        semaphores.get(end._1).get(end._2) !
            new BindLaneMessage(lane, Direction.oppositeDirection(direction), Flow.INCOMING)
        semaphores.get(start._1).get(start._2) ! new BindSemaphoreMessage(semaphores.get(end._1).get(end._2))
        semaphores.get(end._1).get(end._2) ! new BindSemaphoreMessage(semaphores.get(start._1).get(start._2))
        lane
    }

    def calculateMap: CityMap = {
        val cityMapTmp = new CityMap()
        for (i <- 0 until lanes.size()) {
            for (j <- 0 until lanes.get(i).size()) {
                if (lanes.get(i).get(j).containsKey(Direction.NORTH))
                    cityMapTmp.addLane(lanes.get(i).get(j).get(Direction.NORTH),
                        new NextPossibleLanes(lanes.get(i - 1).get(j), Direction.NORTH))
                if (lanes.get(i).get(j).containsKey(Direction.EAST))
                    cityMapTmp.addLane(lanes.get(i).get(j).get(Direction.EAST),
                        new NextPossibleLanes(lanes.get(i).get(j + 1), Direction.EAST))
                if (lanes.get(i).get(j).containsKey(Direction.SOUTH))
                    cityMapTmp.addLane(lanes.get(i).get(j).get(Direction.SOUTH),
                        new NextPossibleLanes(lanes.get(i + 1).get(j), Direction.SOUTH))
                if (lanes.get(i).get(j).containsKey(Direction.WEST))
                    cityMapTmp.addLane(lanes.get(i).get(j).get(Direction.WEST),
                        new NextPossibleLanes(lanes.get(i).get(j - 1), Direction.WEST))
            }
        }
        cityMapTmp
    }

    // Taking the longest path in order to have enough time to test Vehicle
    def chooseStartPosition: ActorRef = {
        val x: Int = 0
        val y: Int = 0
        val direction: Direction = lanes.get(x).get(y).keySet.toArray(
            new Array[Direction](0))(randomGenerator.nextInt(lanes.get(x).get(y).size()))
        lanes.get(x).get(y).get(direction)
    }

    def chooseDestination: util.List[ActorRef] = {
        val x: Int = nSemaphores - 1
        val y: Int = nSemaphores - 1
        val direction: Direction = lanes.get(x).get(y).keySet.toArray(
            new Array[Direction](0))(randomGenerator.nextInt(lanes.get(x).get(y).size()))
        val destination: util.List[ActorRef] = new util.ArrayList[ActorRef]()
        destination.add(lanes.get(x).get(y).get(direction))
        direction match {
            case Direction.NORTH => destination.add(lanes.get(x - 1).get(y).get(Direction.SOUTH))
            case Direction.EAST => destination.add(lanes.get(x).get(y + 1).get(Direction.WEST))
            case Direction.SOUTH => destination.add(lanes.get(x + 1).get(y).get(Direction.NORTH))
            case Direction.WEST => destination.add(lanes.get(x).get(y - 1).get(Direction.EAST))
        }
        destination
    }
}