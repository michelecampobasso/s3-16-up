package messages.init;

import akka.actor.ActorRef;

/**
 * Message sent to bind the City to the MapViewer
 *
 * @author Chiara Ceccarini
 */
public final class InitCityInMapViewerMessage {
    private ActorRef cityActor;

    /**
     * @param cityActor the City Actor that has to be sent to the MapViewer
     */
    public InitCityInMapViewerMessage(ActorRef cityActor) {
        this.cityActor = cityActor;
    }

    public ActorRef getCityActor() {
        return cityActor;
    }
}
