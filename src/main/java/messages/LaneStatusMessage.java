package messages;

import utilities.LaneStatus;

/**
 * Message sent from the Lane to the Semaphore to notify that its state has changed
 *
 * @author Alessio Addimando
 */
public final class LaneStatusMessage {

    private LaneStatus status;

    /**
     * @param status the Lane's status
     */
    public LaneStatusMessage(LaneStatus status) {
        this.status = status;
    }

    public LaneStatus getStatus() {
        return status;
    }
}
