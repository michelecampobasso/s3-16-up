import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import entities.utility.{Logger, MapViewer}
import entities.{Lane, Semaphore}
import messages._
import messages.bind.BindLoggerMessage
import messages.init.{InitLaneMessage, InitLoggerMessage}
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import utilities.enumerations.{LoggerPriority, SemaphoreColor}

/**
  * @author Alberto Mulazzani
  * @author Michele Campobasso
  */

class LaneTest() extends TestKit(ActorSystem("MySpec"))
    with ImplicitSender
    with WordSpecLike
    with BeforeAndAfterAll
    with Matchers {

    val logger: ActorRef = system.actorOf(Props[Logger])
    val laneActor: ActorRef = system.actorOf(Props[Lane])
    val semaphoreFrom: ActorRef = system.actorOf(Props[Semaphore])
    val semaphoreTo: ActorRef = system.actorOf(Props[Semaphore])
    val mapViewer: ActorRef = system.actorOf(Props[MapViewer])

    override def afterAll {
        TestKit.shutdownActorSystem(system)
    }

    override def beforeAll(): Unit = {
        logger ! new InitLoggerMessage(LoggerPriority.LOW)
        laneActor ! new BindLoggerMessage(logger)
        semaphoreFrom ! new BindLoggerMessage(logger)
        semaphoreTo ! new BindLoggerMessage(logger)
    }

    "A Lane actor" must {

        "not reply to init message" in {
            laneActor ! new InitLaneMessage(semaphoreFrom, semaphoreTo, 100)
            expectNoMsg()
        }

        "not reply to SemaphoreColorMessage if green" in {
            laneActor ! new SemaphoreColorMessage(SemaphoreColor.GREEN)
            expectNoMsg()
        }

        "not reply to SemaphoreColorMessage if red" in {
            laneActor ! new SemaphoreColorMessage(SemaphoreColor.RED)
            expectNoMsg()
        }

        "reply to LaneReserveRequestMessage" in {
            laneActor ! new LaneReserveRequestMessage()
            expectMsgClass(classOf[LaneReserveResponseMessage].getSuperclass)
        }

        "not reply to LeftLaneMessage" in {
            laneActor ! new LeftLaneMessage()
            expectNoMsg()
        }

        "not reply to LaneReserveMessage" in {
            laneActor ! new LaneReserveMessage
            expectNoMsg()
        }

        "not reply to LaneNotReserveMessage" in {
            laneActor ! new LaneNotReserveMessage
            expectNoMsg()
        }
    }
}

