package messages;

/**
 * Message sent from the viewer to the city to trigger the creation of the first vehicles after the city initialization
 *
 * @author Enrico Cagnazzo
 */
public final class CreateFirstsVehiclesMessage {
}
