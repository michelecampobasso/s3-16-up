package utilities;

import utilities.enumerations.Direction;

/**
 * Class which represent the Lane position in the city, with its direction
 *
 * @author Chiara Ceccarini
 * @author Alberto Mulazzani
 */
public class LanePosition extends TrafficItemPosition {
    private Direction direction;

    /**
     * @param x         the x position of the TrafficItem in the matrix
     * @param y         the y position if the TrafficItem in the matrix
     * @param direction the direction of the Lane
     */
    public LanePosition(int x, int y, Direction direction) {
        super(x, y);
        this.direction = direction;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction direction) {
        this.direction = direction;
    }
}
