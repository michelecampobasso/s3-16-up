package messages.bind;

import akka.actor.ActorRef;

/**
 * Message necessary to bind the GUI Actor to other Actors of the System. Sent by the City to Semaphores and Lanes.
 *
 * @author Alessio Addimando
 */
public final class BindGuiActorMessage {

    private ActorRef mapViewerRef;

    /**
     * @param mapViewerRef ActorRef of the GUI Actor
     */
    public BindGuiActorMessage(ActorRef mapViewerRef) {
        this.mapViewerRef = mapViewerRef;
    }

    public ActorRef getMapViewerRef() {
        return mapViewerRef;
    }
}
