package messages;

import utilities.CityMap;

/**
 * Message sent from the City to the Vehicle with its map
 *
 * @author Enrico Cagnazzo
 */
public final class MapResponseMessage {
    private final CityMap cityMap;

    /**
     * @param cityMap the map of the City
     */
    public MapResponseMessage(CityMap cityMap) {
        this.cityMap = cityMap;
    }

    public CityMap getCityMap() {
        return cityMap;
    }
}
