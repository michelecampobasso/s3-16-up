package utilities.enumerations;

/**
 * Enum that represents the two possible combination of Directions that a Semaphore can have
 *
 * @author Enrico Cagnazzo
 */
public enum SemaphoreDirection {
    NORTH_SOUTH,
    WEST_EAST;

    @Override
    public String toString() {
        return this.name();
    }

    public SemaphoreDirection changeDirection() {
        return (this.compareTo(NORTH_SOUTH) == 0) ? WEST_EAST : NORTH_SOUTH;
    }
}
