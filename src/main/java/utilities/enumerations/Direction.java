package utilities.enumerations;

/**
 * Direction Enum that represents the four possible directions of Lane Actors in a Semaphore Actor.
 *
 * @author Alessio Addimando
 * @author Alberto Mulazzani
 * @author Enrico Cagnazzo
 */
public enum Direction {
    NORTH,
    SOUTH,
    EAST,
    WEST;

    @Override
    public String toString() {
        return this.name();
    }

    /**
     * @param direction the current Direction
     * @return the opposite Direction.
     */
    public static Direction oppositeDirection(Direction direction) {
        if (direction == Direction.NORTH) return Direction.SOUTH;
        if (direction == Direction.SOUTH) return Direction.NORTH;
        if (direction == Direction.EAST) return Direction.WEST;
        return Direction.EAST;
    }
}
