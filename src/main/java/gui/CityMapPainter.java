package gui;

import akka.actor.ActorRef;
import utilities.LanePosition;
import utilities.LaneStatus;
import utilities.TrafficItemPosition;
import utilities.enumerations.Direction;
import utilities.enumerations.SemaphoreDirection;
import utilities.enumerations.SemaphoreStrategy;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

/**
 * Class that draws and updates Semaphores, Lanes and their congestion.
 *
 * @author Chiara Ceccarini
 * @author Alberto Mulazzani
 * @author Enrico Cagnazzo
 */
public class CityMapPainter extends JFrame {

    private int offset;

    private int lanesWidth;
    private int lanesLength;
    private int semaphoreSize;
    private int crossSize;
    private int padding;

    private Map<ActorRef, LanePosition> lanePositionMap = new HashMap<>();
    private Map<ActorRef, Color> lanesColors = new HashMap<>();

    private Map<ActorRef, TrafficItemPosition> semaphorePositionMap = new HashMap<>();
    private Map<ActorRef, SemaphoreStrategy> semaphoresStrategies = new HashMap<>();
    private Map<ActorRef, SemaphoreDirection> semaphoresStatus = new HashMap<>();

    private Graphics2D initialComponent;

    public CityMapPainter() {
        pack();
    }

    /**
     * Method to prints the map in the view
     *
     * @param g          the Graphics of the JFrame.paint
     * @param lanes      the lanes of the City
     * @param semaphores the semaphores of the City
     * @param width      the width of the window
     * @param height     the height of the window
     */
    void paintMap(Graphics g, ArrayList<ArrayList<Map<Direction, ActorRef>>> lanes,
                  ArrayList<ArrayList<ActorRef>> semaphores, int width, int height) {

        calculateDimensions(semaphores.size(), width, height);
        this.initialComponent = (Graphics2D) g;
        printLanes(initialComponent, lanes);
        printSemaphores(semaphores);
    }

    /**
     * Method which prints the Semaphores in the map
     *
     * @param semaphores the semaphores of the City
     */
    private void printSemaphores(ArrayList<ArrayList<ActorRef>> semaphores) {
        for (int i = 0; i < semaphores.size(); i++) {
            for (int j = 0; j < semaphores.get(i).size(); j++) {
                ActorRef semaphore = semaphores.get(i).get(j);
                if (!semaphorePositionMap.containsKey(semaphore))
                    semaphorePositionMap.put(semaphore, new TrafficItemPosition(i, j));
                if (!semaphoresStrategies.containsKey(semaphore))
                    semaphoresStrategies.put(semaphore, SemaphoreStrategy.DEFAULT);
                if (!semaphoresStatus.containsKey(semaphore))
                    semaphoresStatus.put(semaphore, SemaphoreDirection.NORTH_SOUTH);
                printSemaphoreStrategy(i, j, semaphoresStrategies.get(semaphore));
                printSemaphoreLights(i, j, semaphoresStatus.get(semaphore));
            }
        }
    }

    /**
     * Method which prints the Lanes
     *
     * @param initialComponent the Graphics of the JFrame.paint
     * @param lanes            the Lanes of the city
     */
    private void printLanes(Graphics2D initialComponent, ArrayList<ArrayList<Map<Direction, ActorRef>>> lanes) {
        initialComponent.setStroke(new BasicStroke(lanesWidth));
        IntStream.range(0, lanes.size()).forEach(i ->
                IntStream.range(0, lanes.get(i).size()).forEach(j ->
                        lanes.get(i).get(j).forEach(((direction, lane) -> {
                            fillLaneMaps(lane, i, j, direction);
                            printLane(lane, i, j, direction);
                        }))));
    }

    /**
     * Method that calculates the dimension of the City's element
     *
     * @param semaphoresNumber the number of Semaphores
     * @param width            the width of the window
     * @param height           the height of the window
     */
    private void calculateDimensions(int semaphoresNumber, int width, int height) {
        crossSize = (height - offset) / (semaphoresNumber + ((semaphoresNumber - 1) * 8 / 3));
        semaphoreSize = crossSize / 3;
        lanesLength = 2 * crossSize;
        lanesWidth = crossSize / 2;
        padding = (width - (semaphoresNumber * crossSize + (semaphoresNumber - 1) * lanesLength)) / 2;
        offset = (height + crossSize - (semaphoresNumber * crossSize + (semaphoresNumber - 1) * lanesLength)) / 2;
    }

    /**
     * Method that updates the color of a Lane
     *
     * @param lane   the Lane to update
     * @param status the status of the Lane
     */
    public void updateLane(ActorRef lane, LaneStatus status) {
        if (initialComponent != null) {
            LanePosition position = lanePositionMap.get(lane);
            double congestion = status.getCongestion();
            Color color = congestion == 0 ? new Color(157, 165, 158) :
                    congestion < 0.2 ? new Color(75, 198, 83) :
                            congestion < 0.4 ? new Color(224, 229, 73) :
                                    congestion < 0.7 ? new Color(229, 155, 64) :
                                            new Color(229, 68, 68);
            lanesColors.put(lane, color);
            initialComponent.setStroke(new BasicStroke(lanesWidth));
            printLane(lane, position.getX(), position.getY(), position.getDirection());
        }
    }

    /**
     * Method that updates the color of the Semaphore
     *
     * @param semaphore      the semaphore to update
     * @param greenDirection the direction that has the green light
     */
    public void updateSemaphoreLights(ActorRef semaphore, SemaphoreDirection greenDirection) {
        int i = semaphorePositionMap.get(semaphore).getX();
        int j = semaphorePositionMap.get(semaphore).getY();
        semaphoresStatus.put(semaphore, greenDirection);
        printSemaphoreLights(i, j, greenDirection);
    }

    /**
     * Method that updates the Semaphore's Strategy
     *
     * @param semaphore the Semaphore to update
     * @param strategy  the new Strategy of the Semaphore
     */
    public void updateSemaphoreStrategy(ActorRef semaphore, SemaphoreStrategy strategy) {
        int i = semaphorePositionMap.get(semaphore).getX();
        int j = semaphorePositionMap.get(semaphore).getY();
        semaphoresStrategies.put(semaphore, strategy);
        printSemaphoreStrategy(i, j, strategy);
    }

    /**
     * Check if lanePositionMap and lanesColors have the lane, if not the lane is added
     *
     * @param lane the lane checked
     */
    private void fillLaneMaps(ActorRef lane, int i, int j, Direction direction) {
        if (!lanePositionMap.containsKey(lane))
            lanePositionMap.put(lane, new LanePosition(i, j, direction));
        if (!lanesColors.containsKey(lane))
            lanesColors.put(lane, new Color(157, 165, 158));
    }

    /**
     * Sets the color and print the lane
     */
    private void printLane(ActorRef lane, int i, int j, Direction direction) {
        setLaneColor(lane);
        switch (direction) {
            case NORTH:
                printLaneNorth(i, j);
                break;
            case WEST:
                printLaneWest(i, j);
                break;
            case SOUTH:
                printLaneSouth(i, j);
                break;
            case EAST:
                printLaneEast(i, j);
                break;
        }
    }

    /**
     * Method to set the color of the Lane
     *
     * @param lane the Lane that should have its color set
     */
    private void setLaneColor(ActorRef lane) {
        initialComponent.setColor(lanesColors.get(lane));
    }

    /**
     * Prints the Lane from North to South
     *
     * @param i i position in the matrix
     * @param j j position in the matrix
     */
    private void printLaneNorth(int i, int j) {
        if (initialComponent != null) {
            initialComponent.drawLine((crossSize + lanesLength) * j + lanesWidth + padding,
                    (crossSize + lanesLength) * i - lanesLength + offset,
                    (crossSize + lanesLength) * j + lanesWidth + padding,
                    (crossSize + lanesLength) * i + offset - lanesWidth);
        }
    }

    /**
     * Prints the Lane from West to East
     *
     * @param i i position in the matrix
     * @param j j position in the matrix
     */
    private void printLaneWest(int i, int j) {
        if (initialComponent != null) {
            initialComponent.drawLine((crossSize + lanesLength) * j - lanesLength + padding,
                    (crossSize + lanesLength) * i + offset,
                    (crossSize + lanesLength) * j + padding - lanesWidth,
                    (crossSize + lanesLength) * i + offset);
        }
    }

    /**
     * Prints the Lane from South to North
     *
     * @param i i position in the matrix
     * @param j j position in the matrix
     */
    private void printLaneSouth(int i, int j) {
        if (initialComponent != null) {
            initialComponent.drawLine((crossSize + lanesLength) * j + padding,
                    (crossSize + lanesLength) * i + crossSize + offset,
                    (crossSize + lanesLength) * j + padding,
                    (crossSize + lanesLength) * i + crossSize + lanesLength + offset - lanesWidth);
        }
    }

    /**
     * Prints the Lane from East to West
     *
     * @param i i position in the matrix
     * @param j j position in the matrix
     */
    private void printLaneEast(int i, int j) {
        if (initialComponent != null) {
            initialComponent.drawLine((crossSize + lanesLength) * j + crossSize + padding,
                    (crossSize + lanesLength) * i + lanesWidth + offset,
                    (crossSize + lanesLength) * j + crossSize + lanesLength + padding - lanesWidth,
                    (crossSize + lanesLength) * i + lanesWidth + offset);
        }
    }

    /**
     * Prints the Semaphore's Strategy
     *
     * @param i        i position in the matrix
     * @param j        j position in the matrix
     * @param strategy the Semaphore's Strategy
     */
    private void printSemaphoreStrategy(int i, int j, SemaphoreStrategy strategy) {
        if (initialComponent != null) {
            initialComponent.setColor(strategy == SemaphoreStrategy.DEFAULT ? Color.WHITE :
                    strategy == SemaphoreStrategy.AUTONOMOUS ? new Color(74, 209, 224) :
                            Color.BLUE);
            initialComponent.fillRect(padding + (crossSize + lanesLength) * j + semaphoreSize - lanesWidth / 2,
                    offset + (crossSize + lanesLength) * i + semaphoreSize - lanesWidth / 2,
                    semaphoreSize, semaphoreSize);
        }
    }

    /**
     * Prints the Semaphore's color
     *
     * @param i              i position in the matrix
     * @param j              j position in the matrix
     * @param greenDirection the direction that has the green light
     */
    private void printSemaphoreLights(int i, int j, SemaphoreDirection greenDirection) {
        if (initialComponent != null) {
            initialComponent.setColor(greenDirection == SemaphoreDirection.NORTH_SOUTH ? Color.GREEN : Color.RED);
            initialComponent.fillRect(padding + (crossSize + lanesLength) * j + semaphoreSize - lanesWidth / 2,
                    offset + (crossSize + lanesLength) * i - lanesWidth / 2, semaphoreSize, semaphoreSize);
            initialComponent.fillRect(padding + (crossSize + lanesLength) * j + semaphoreSize - lanesWidth / 2,
                    offset + (crossSize + lanesLength) * i + semaphoreSize * 2 - lanesWidth / 2,
                    semaphoreSize, semaphoreSize);

            initialComponent.setColor(greenDirection == SemaphoreDirection.WEST_EAST ? Color.GREEN : Color.RED);
            initialComponent.fillRect(padding + (crossSize + lanesLength) * j - lanesWidth / 2,
                    offset + (crossSize + lanesLength) * i + semaphoreSize - lanesWidth / 2,
                    semaphoreSize, semaphoreSize);
            initialComponent.fillRect(padding + (crossSize + lanesLength) * j + semaphoreSize * 2 - lanesWidth / 2,
                    offset + (crossSize + lanesLength) * i + semaphoreSize - lanesWidth / 2,
                    semaphoreSize, semaphoreSize);
        }
    }
}
