package messages;

import utilities.enumerations.SemaphoreColor;

/**
 * Message sent from the Lane to the Vehicle and from the Semaphore to the Lane to notify the Semaphore's color
 *
 * @author Alessio Addimando
 */
public final class SemaphoreColorMessage {
    private SemaphoreColor status;

    /**
     * @param status the Semaphore's status
     */
    public SemaphoreColorMessage(SemaphoreColor status) {
        this.status = status;
    }

    public SemaphoreColor getStatus() {
        return status;
    }

}
