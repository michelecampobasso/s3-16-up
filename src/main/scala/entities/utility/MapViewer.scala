package entities.utility

import java.util
import java.util.concurrent.TimeUnit

import akka.actor.{AbstractActor, ActorRef, Props}
import entities.dummyTestUtility.LoadController
import gui._
import messages._
import messages.gui._
import messages.init.{InitCityInMapViewerMessage, InitCityMessage, InitLoadControllerMessage, InitViewMessage}
import messages.utility.{RequestStatisticsFileMessage, StartMessage}
import utilities.enumerations.{Direction, TrafficLoadState}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.FiniteDuration

/**
  * MapViewer Actor. It receives messages from all the other Actors in the System and update the GUI
  *
  * @author Alessio Addimando
  * @author Enrico Cagnazzo
  * @author Chiara Ceccarini
  * @author Alberto Mulazzani
  */
class MapViewer extends AbstractActor {

    private var divider: Int = _

    private var mapView: MapView = _

    private var lanes: util.ArrayList[util.ArrayList[util.Map[Direction, ActorRef]]] = _
    private var semaphores: util.ArrayList[util.ArrayList[ActorRef]] = _

    private var statistician: ActorRef = _
    private var cityActor: ActorRef = _

    private var vehiclesToAdd: Int = 0

    private val loadController: ActorRef = context.actorOf(Props[LoadController])

    private var loadState: TrafficLoadState = TrafficLoadState.LOW

    /**
      * The MapViewer actor can receive and handle these messages:<br>
      *
      * InitViewMessage<br>
      * ChangeNumberOfVehiclesGuiMessage<br>
      * ChangeLaneGuiMessage<br>
      * ChangeSemaphoreLightsGuiMessage<br>
      * ChangeSemaphoreStrategyGuiMessage<br>
      * AddVehicleMessage<br>
      * RequestStatisticsFileMessage<br>
      * InitCityMessage <br>
      * StartMessage<br>
      * InitCityInMapViewerMessage <br>
      * ChangeTrafficLoadMessage<br>
      * DestinationReachedMessage<br>
      */
    override def receive = {

        case message: InitViewMessage =>
            lanes = message.getLanes
            semaphores = message.getSemaphores
            statistician = message.getStatistician
            mapView = new MapView(new CityMapPainter(), message.getLanes, message.getSemaphores, self)
            mapView updateNumberOfVehicles message.getNumberOfVehicles
            mapView.updateTrafficLoad(loadState)
            loadController ! new InitLoadControllerMessage(cityActor)
            divider = (25 * (20 + 2 * semaphores.size())) / 70
            vehiclesToAdd = message.getNumberOfVehicles / divider
            initScheduler

        case message: ChangeNumberOfVehiclesGuiMessage =>
            mapView updateNumberOfVehicles message.getNumberOfVehicles

        case message: ChangeLaneGuiMessage =>
            mapView updateLane(message.getLane, message.getStatus)

        case message: ChangeSemaphoreLightsGuiMessage =>
            mapView updateSemaphoreLights(message.getSemaphore, message.getGreenDirection)

        case message: ChangeSemaphoreStrategyGuiMessage =>
            mapView updateSemaphoreStrategy(message.getSemaphore, message.getStrategy)

        case message: AddVehicleMessage =>
            cityActor forward message

        case message: RequestStatisticsFileMessage =>
            statistician forward message

        case message: InitCityMessage =>
            cityActor forward message

        case message: StartMessage =>
            cityActor forward message
            cityActor ! new CreateFirstsVehiclesMessage

        case message: InitCityInMapViewerMessage =>
            cityActor = message.getCityActor

        case message: ChangeTrafficLoadMessage =>
            loadState = message.getState
            mapView.updateTrafficLoad(loadState)

        case _: DestinationReachedMessage =>
            mapView.updateNumberOfArrivedVehicles()
    }

    /**
      * Method to init the scheduler that has to send message in order to add vehicles
      *
      * @return the scheduler cancellable
      */
    private def initScheduler = {
        context.system.scheduler.schedule(
            FiniteDuration.apply(1000, TimeUnit.MILLISECONDS),
            FiniteDuration.apply(500 * semaphores.size(), TimeUnit.MILLISECONDS),
            loadController,
            new AddVehicleMessage(vehiclesToAdd)
        )
    }
}