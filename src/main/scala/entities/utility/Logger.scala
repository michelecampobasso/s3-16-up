package entities.utility

import akka.actor.AbstractActor
import messages.init.InitLoggerMessage
import messages.utility.PrintLogMessage
import utilities.enumerations.LoggerPriority

/**
  * Logger Actor. It receives messages from all the other Actors in the System and prints them according to the
  * message's priority and his priorityToPrint
  *
  * @author Alberto Mulazzani
  */
class Logger extends AbstractActor() {

    private var priorityToPrint: LoggerPriority = LoggerPriority.DISABLED

    /**
      * The Logger Actor can receive and handle these messages:<br>
      *
      * InitLoggerMessage<br>
      * PrintLogMessage<br>
      */
    override def receive = {

        case message: InitLoggerMessage => priorityToPrint = message.getPriorityToPrint

        case message: PrintLogMessage =>
            if (message.getPriority.compareTo(priorityToPrint) <= 0) println(message.getMessage)
    }
}
