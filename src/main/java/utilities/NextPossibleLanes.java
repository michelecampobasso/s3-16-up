package utilities;

import akka.actor.ActorRef;
import utilities.enumerations.Direction;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Class that represent the next possible Lanes in the path
 *
 * @author Enrico Cagnazzo
 */
public class NextPossibleLanes {
    private List<ActorRef> nextLanes = new ArrayList<>();

    /**
     * @param nextPossibleLines map that contains the next possible Lanes
     * @param origin            the origin position
     */
    public NextPossibleLanes(Map<Direction, ActorRef> nextPossibleLines, Direction origin) {
        if (nextPossibleLines.containsKey(Direction.NORTH) && origin != Direction.SOUTH)
            this.nextLanes.add(nextPossibleLines.get(Direction.NORTH));
        if (nextPossibleLines.containsKey(Direction.EAST) && origin != Direction.WEST)
            this.nextLanes.add(nextPossibleLines.get(Direction.EAST));
        if (nextPossibleLines.containsKey(Direction.SOUTH) && origin != Direction.NORTH)
            this.nextLanes.add(nextPossibleLines.get(Direction.SOUTH));
        if (nextPossibleLines.containsKey(Direction.WEST) && origin != Direction.EAST)
            this.nextLanes.add(nextPossibleLines.get(Direction.WEST));
    }

    /**
     * @return the next Lane in the path
     */
    public List<ActorRef> getNextLanes() {
        return nextLanes;
    }

}
