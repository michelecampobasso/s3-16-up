import akka.actor.{ActorSystem, Props}
import entities.City
import entities.dummyTestUtility.ComparisonViewer
import entities.utility.MapViewer
import gui.MapForm

/**
  * The main class of the Application
  *
  * @author Alessio Addimando
  * @author Michele Campobasso
  */
object StartApplication extends App {
    val system: ActorSystem = ActorSystem.create("System")
    new MapForm(Props[MapViewer], Props[ComparisonViewer], Props[City], system)
}

