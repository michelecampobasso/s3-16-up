package entities

import akka.actor.ActorRef
import messages._
import messages.bind.{BindGuiActorMessage, BindLoggerMessage, BindStatisticianMessage}
import messages.init.InitLaneMessage
import messages.utility.LaneTrafficInformationMessage
import utilities._
import utilities.enumerations.{LaneReserveResponse, LoggerPriority, SemaphoreColor}

import scala.collection.mutable

/**
  * Lane Actor. It has a Flow and goes from SemaphoreFrom to SemaphoreTo. Vehicle Actors are linked to the
  * Lane Actor in his vehicles Map.
  *
  * @author Chiara Ceccarini
  * @author Alberto Mulazzani
  * @author Alessio Addimando
  * @author Michele Campobasso
  * @author Enrico Cagnazzo
  *
  */
class Lane extends AbstractActorWithUpuautUtils {

    private var semaphoreFrom: ActorRef = _
    private var semaphoreTo: ActorRef = _
    private var statistician: ActorRef = _

    private var vehicles: mutable.ArrayBuffer[(ActorRef, ActorRef)] = new mutable.ArrayBuffer[(ActorRef, ActorRef)]()
    private var vehiclesInWait: mutable.Set[ActorRef] = new mutable.HashSet[ActorRef]()
    private val alertedVehicles: mutable.Set[ActorRef] = new mutable.HashSet[ActorRef]()

    private var capacity: Int = 0
    private var lastTrafficPercentage: Double = 0.0
    private var lanternColor: SemaphoreColor = SemaphoreColor.RED

    /**
      * Receive method of the Lane Actor
      *
      * The Lane Actor can receive and handle these messages:<br>
      * InitLaneMessage<br>
      * BindLoggerMessage<br>
      * BindGuiActorMessage<br>
      * BindStatisticianMessage<br>
      * LaneReserveRequestMessage<br>
      * LaneReserveMessage<br>
      * LaneNotReserveMessage<br>
      * LeftLaneMessage<br>
      * SemaphoreColorMessage<br>
      *
      */
    override def receive = {

        case message: InitLaneMessage =>
            semaphoreFrom = message.getSemaphoreFrom
            semaphoreTo = message.getSemaphoreTo
            capacity = message.getCapacity

        case message: BindLoggerMessage =>
            loggerRef = message.getLoggerRef

        case message: BindGuiActorMessage =>
            viewer = message.getMapViewerRef

        case message: BindStatisticianMessage => statistician = message.getStatistician

        case message: LaneReserveRequestMessage =>
            printLog("LaneReserveRequestMessage received", LoggerPriority.LOW)
            if (vehicles.size < capacity) {
                sender() ! new LaneReserveResponseMessage(LaneReserveResponse.FREE)
                vehiclesInWait -= sender
                if (!containsVehicle(sender))
                    vehicles += sender() -> message.getNextLane
                updateTrafficPercentage()
            } else {
                sender() ! new LaneReserveResponseMessage(LaneReserveResponse.BLOCKED)
                vehiclesInWait += sender
            }

        case _: LaneReserveMessage =>
            printLog("LaneReserveMessage received", LoggerPriority.LOW)
            alertOneDirection(getNextDirection(sender))

        case _: LaneNotReserveMessage =>
            printLog("LaneNotReserveMessage received", LoggerPriority.LOW)
            removeVehicle(sender)

        case _: LeftLaneMessage =>
            printLog("LeftLaneMessage received", LoggerPriority.LOW)
            val freedDirection = getNextDirection(sender)
            removeVehicle(sender)
            updateTrafficPercentage()
            alertOneDirection(freedDirection)
            for (vehicle <- vehiclesInWait)
                vehicle ! new NotMoreFullMessage

        case message: SemaphoreColorMessage =>
            printLog("SemaphoreColorMessage received", LoggerPriority.LOW)
            lanternColor = message.getStatus
            alertAllDirections()
    }

    /**
      * Calculates the new traffic percentage and if it is changed updates the adjacent semaphores and the GUI
      */
    private def updateTrafficPercentage(): Unit = {
        val currentTrafficPercentage = calculateTrafficPercentage
        if (lastTrafficPercentage != currentTrafficPercentage) {
            val laneStatus = new LaneStatus(currentTrafficPercentage)
            semaphoreFrom ! new LaneStatusMessage(laneStatus)
            semaphoreTo ! new LaneStatusMessage(laneStatus)
            updateLaneCongestionInGui(laneStatus)
            lastTrafficPercentage = currentTrafficPercentage
            if (statistician != null)
                statistician ! new LaneTrafficInformationMessage(new LaneIdentifier(semaphoreFrom, semaphoreTo),
                    currentTrafficPercentage)
        }
    }

    /**
      * Calculates the traffic percentage of a Lane
      *
      * @return the percentage
      */
    private def calculateTrafficPercentage: Double =
        Math.round(vehicles.size * 10.0 / capacity) / 10.0

    /**
      * Get the next direction of a specified vehicle
      * Returns itself if something goes wrong but it should not happen
      *
      * @param vehicleToSearch the vehicle to search
      * @return the next direction in the path
      */
    private def getNextDirection(vehicleToSearch: ActorRef): ActorRef = {
        vehicles.foreach((vehicle: (ActorRef, ActorRef)) =>
            if (vehicle._1 == vehicleToSearch)
                return vehicle._2
        )
        self
    }

    /**
      * Removes a vehicle from the list of all the vehicles
      *
      * @param vehicleToRemove the vehicle to remove
      */
    private def removeVehicle(vehicleToRemove: ActorRef): Unit = {
        alertedVehicles -= vehicleToRemove
        vehicles.foreach((vehicle: (ActorRef, ActorRef)) =>
            if (vehicle._1 == vehicleToRemove) {
                vehicles -= vehicle
                return
            }
        )
    }

    /**
      * Alert the first vehicle that want to move to the specified direction in case
      * the direction isn't null and the vehicle wasn't already alerted
      *
      * @param freedDirection the next direction
      */
    private def alertOneDirection(freedDirection: ActorRef): Unit = {
        if (freedDirection != null) {
            vehicles.foreach((vehicle: (ActorRef, ActorRef)) =>
                if (vehicle._2 == freedDirection) {
                    if (!alertedVehicles.contains(vehicle._1)) {
                        vehicle._1 ! new SemaphoreColorMessage(lanternColor)
                        alertedVehicles.add(vehicle._1)
                    }
                    return
                }
            )
        }
    }

    /**
      * Alerts the first vehicles moving to the three next direction and who has null as next direction
      */
    private def alertAllDirections(): Unit = {
        val alertedDirection = new mutable.HashSet[ActorRef]
        alertedVehicles.clear()
        vehicles.foreach((vehicle: (ActorRef, ActorRef)) =>
            if (!alertedDirection.contains(vehicle._2)) {
                vehicle._1 ! new SemaphoreColorMessage(lanternColor)
                alertedVehicles.add(vehicle._1)
                if (vehicle._2 != null)
                    alertedDirection += vehicle._2
            }
        )
    }

    /**
      * Checks if the vehicle is inside the list of the vehicles in the lane
      *
      * @param vehicleToSearch the vehicle to search
      * @return the result of check
      */
    private def containsVehicle(vehicleToSearch: ActorRef): Boolean = {
        vehicles.foreach((vehicle: (ActorRef, ActorRef)) =>
            if (vehicle._1 == vehicleToSearch)
                return true
        )
        false
    }
}
