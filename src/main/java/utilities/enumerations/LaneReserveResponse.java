package utilities.enumerations;

/**
 * Enum that represents the Availability of a Lane Actor.
 *
 * @author Alberto Mulazzani
 */
public enum LaneReserveResponse {
    FREE,
    BLOCKED;

    @Override
    public String toString() {
        return this.name();
    }
}
