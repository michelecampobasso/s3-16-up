# **UPUAUT**



#### This software has been developed by: 


#### Addimando Alessio 		(alessio.addimando@studio.unibo.it)
#### Cagnazzo Enrico 		(enrico.cagnazzo@studio.unibo.it)
#### Campobasso Michele 	(michele.campobasso2@studio.unibo.it)
#### Ceccarini Chiara 		(chiara.ceccarini5@studio.unibo.it)
#### Mulazzani Alberto 		(alberto.mulazzani@studio.unibo.it)



#### The goal of this software is to model an intelligent system that manages traffic loads and improves road conditions.

# SETUP

## Download repository

#### Choose your favourite GIT tool and download this repository in local.

## IntelliJ IDEA

### Before beginning, ensure you have installed Gradle 4.0.1 or above.

#### Open IntelliJ Idea > Import Project > Select directory... > Select radio button on "Import project from external model" and select Gradle.

### In Gradle's view: 

#### - unpin "Create separate module per source set";
#### - pin "Use auto-import";
#### - select radio button "Use gradle wrapper task configuration";
#### - ensure you have jdk1.8.0 or above.

#### Give Gradle some time for downloading all necessary packages, then open src/main/scala/StartApplication.scala and run it.

#### **NOTE: Sources, report, Product Backlog and Sprint Backlogs are available in Downloads section.**
#### **Is also available in Downloads section an executable .jar file of the application.**

#### Enjoy!




