package messages;

import akka.actor.ActorRef;

/**
 * Message sent from a Vehicle Actor to a Lane Actor to reserve a spot in the Lane Actor.
 *
 * @author Enrico Cagnazzo
 * @author Alberto Mulazzani
 * @author Chiara Ceccarini
 */
public final class LaneReserveRequestMessage {

    private ActorRef nextLane = null;

    public LaneReserveRequestMessage() {
    }

    /**
     * @param nextLane the Lane where the Vehicle wants to access.
     */
    public LaneReserveRequestMessage(ActorRef nextLane) {
        this.nextLane = nextLane;
    }

    public ActorRef getNextLane() {
        return nextLane;
    }
}
