package messages;

import utilities.SemaphoreStatus;

/**
 * Message sent from the Semaphore to the one that has requested its traffic information
 *
 * @author Enrico Cagnazzo
 */
public final class TrafficDetailsResponseMessage {
    private SemaphoreStatus semaphoreStatus;

    /**
     * @param semaphoreStatus the status of the Semaphore
     */
    public TrafficDetailsResponseMessage(SemaphoreStatus semaphoreStatus) {
        this.semaphoreStatus = semaphoreStatus;
    }

    public SemaphoreStatus getSemaphoreStatus() {
        return semaphoreStatus;
    }
}
