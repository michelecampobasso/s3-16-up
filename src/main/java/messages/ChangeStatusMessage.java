package messages;

/**
 * Message sent from the Semaphore to itself to change its own status
 *
 * @author Alessio Addimando
 */
public final class ChangeStatusMessage {
}
