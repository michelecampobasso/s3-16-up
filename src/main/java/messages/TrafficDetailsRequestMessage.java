package messages;

/**
 * Message sent from a Semaphore to the adjacent Semaphores to request traffic details
 *
 * @author Michele Campobasso
 */
public final class TrafficDetailsRequestMessage {

    public TrafficDetailsRequestMessage() {
    }
}
