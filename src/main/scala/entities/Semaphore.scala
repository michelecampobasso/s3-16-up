package entities

import akka.actor.ActorRef
import messages._
import messages.bind.{BindGuiActorMessage, BindLaneMessage, BindLoggerMessage, BindSemaphoreMessage}
import messages.utility.IgnoreTrafficConditionsMessage
import utilities.enumerations._
import utilities.{LaneStatus, SemaphoreStatus}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * Semaphore Actor that communicates with Lane Actors and change his trafficFlowStrategy according to
  * his lanes congestion.
  *
  * @author Alberto Mulazzani
  * @author Chiara Ceccarini
  * @author Michele Campobasso
  * @author Alessio Addimando
  * @author Enrico Cagnazzo
  */
object Semaphore {
    val CONGESTION_THRESHOLD = 0.6
    val SINGLE_TIME_CHANGE_LANTERN_COLOR: Long = 5000
    val DOUBLE_TIME_CHANGE_LANTERN_COLOR: Long = SINGLE_TIME_CHANGE_LANTERN_COLOR * 2
    val SHORT_TIME_CHANGE_LANTERN_COLOR: Long = SINGLE_TIME_CHANGE_LANTERN_COLOR * 2 / 5
    val LONG_TIME_CHANGE_LANTERN_COLOR: Long = SINGLE_TIME_CHANGE_LANTERN_COLOR * 8 / 5
}

class Semaphore extends AbstractActorWithUpuautUtils {

    private val incomingLanes: mutable.HashMap[ActorRef, Direction] = new mutable.HashMap()
    private val outgoingLanes: mutable.HashMap[ActorRef, Direction] = new mutable.HashMap()
    private val incomingLanesStatus: mutable.HashMap[ActorRef, LaneStatus] = new mutable.HashMap()
    private val outgoingLanesStatus: mutable.HashMap[ActorRef, LaneStatus] = new mutable.HashMap()

    private var adjacentLanes: ListBuffer[ActorRef] = new ListBuffer()
    private var adjacentSemaphores: ListBuffer[ActorRef] = new ListBuffer()
    private val adjacentSemaphoreStatus: mutable.HashMap[ActorRef, SemaphoreStatus] = new mutable.HashMap()

    private var currentState: SemaphoreDirection = SemaphoreDirection.NORTH_SOUTH
    private var currentTrafficFlowStrategy: SemaphoreStrategy =
        SemaphoreStrategy.createDefault(context.system.scheduler, self)

    private var previousEWCongestionRate: Double = 0.0
    private var previousSNCongestionRate: Double = 0.0

    private var ignoreTrafficConditions: Boolean = false

    /**
      * A Semaphore Actor can accept and handle these messages:<br>
      *
      * BindLoggerMessage<br>
      * BindGuiActorMessage<br>
      * BindLaneMessage<br>
      * BindSemaphoreMessage<br>
      * IgnoreTrafficConditionsMessage<br>
      * TrafficDetailsRequestMessage<br>
      * TrafficDetailsResponseMessage<br>
      * ChangeStatusMessage<br>
      * LaneStatusMessage<br>
      */
    override def receive = {

        case message: BindLoggerMessage => loggerRef = message.getLoggerRef

        case message: BindGuiActorMessage => viewer = message.getMapViewerRef

        case message: BindLaneMessage =>
            message.getFlow match {
                case Flow.INCOMING => incomingLanes.put(message.getLane, message.getDirection)
                case Flow.OUTGOING => outgoingLanes.put(message.getLane, message.getDirection)
            }
            adjacentLanes += message.getLane

        case message: BindSemaphoreMessage =>
            if (!adjacentSemaphores.contains(message.getSemaphore))
                adjacentSemaphores += message.getSemaphore

        case _: IgnoreTrafficConditionsMessage =>
            ignoreTrafficConditions = true

        case _: TrafficDetailsRequestMessage =>
            printLog("TrafficDetailsRequestMessage received from " + sender().path.name, LoggerPriority.LOW)
            getContext().sender() ! new TrafficDetailsResponseMessage(
                new SemaphoreStatus(incomingLanesStatus, outgoingLanesStatus))

        case message: TrafficDetailsResponseMessage =>
            printLog("TrafficDetailsResponseMessage received from " + sender().path.name, LoggerPriority.LOW)
            adjacentSemaphoreStatus.put(sender(), message.getSemaphoreStatus)

            if (adjacentSemaphoreStatus.size == adjacentSemaphores.size) {
                val ratios: (Double, Double) = calculateIncomingTrafficFromOtherSemaphores()
                currentTrafficFlowStrategy.asInstanceOf[CollaborativeSemaphore].
                    setRatios(ratioEW = ratios._1, ratioSN = ratios._2)
                currentTrafficFlowStrategy.updateTimers()
                adjacentSemaphoreStatus.clear()
            }

        case _: ChangeStatusMessage =>
            printLog("ChangeStatusMessage received", LoggerPriority.LOW)
            currentState = currentState.changeDirection()
            alertIncomingLanes()
            updateSemaphoreLightsInGui(currentState)
            if (!ignoreTrafficConditions) {
                val congestion: (Double, Double) = calculateIncomingTrafficCongestion()
                updateSemaphoreStrategy(congestionEW = congestion._1, congestionSN = congestion._2)
            }

        case message: LaneStatusMessage =>
            printLog("LaneStatusMessage received from " + sender().path.name, LoggerPriority.LOW)
            if (incomingLanes.contains(sender()))
                incomingLanesStatus(sender()) = message.getStatus
            else if (outgoingLanes.contains(sender()))
                outgoingLanesStatus(sender()) = message.getStatus
    }

    /**
      * Alert the incoming lanes of the new lantern color
      */
    private def alertIncomingLanes(): Unit = {
        for (incomingLane <- incomingLanes) {
            val direction = incomingLane._2
            incomingLane._1 ! new SemaphoreColorMessage(
                if (((direction == Direction.NORTH || direction == Direction.SOUTH) &&
                    currentState == SemaphoreDirection.NORTH_SOUTH) ||
                    ((direction == Direction.EAST || direction == Direction.WEST) &&
                        currentState == SemaphoreDirection.WEST_EAST))
                    SemaphoreColor.GREEN
                else SemaphoreColor.RED
            )
        }
    }

    /**
      * Checks if the last congestion is the same of the current (same direction).
      *
      * @param congestionEW the congestion in East-West direction
      * @param congestionSN the congestion in South-North direction
      * @return Boolean
      */
    private def changeSemaphoreCongestion(congestionEW: Double, congestionSN: Double): Boolean =
        (congestionEW >= Semaphore.CONGESTION_THRESHOLD && previousSNCongestionRate >= Semaphore.CONGESTION_THRESHOLD) ||
            (congestionSN >= Semaphore.CONGESTION_THRESHOLD && previousEWCongestionRate >= Semaphore.CONGESTION_THRESHOLD)

    /**
      * Calculate the traffic incoming using the status of the incoming lanes
      *
      * @return (congestionEW,congestionSN) a tuple with the two congestion levels
      */
    private def calculateIncomingTrafficCongestion(): (Double, Double) = {
        // Checking if the current strategy is appropriate to the traffic congestion.
        var congestionEW = 0.0
        var EWLanesCount = 0
        var congestionSN = 0.0
        var SNLanesCount = 0
        for (incomingLaneStatus <- incomingLanesStatus) {
            incomingLanes(incomingLaneStatus._1) match {
                case Direction.EAST | Direction.WEST =>
                    congestionEW += incomingLaneStatus._2.getCongestion; EWLanesCount += 1
                case Direction.SOUTH | Direction.NORTH =>
                    congestionSN += incomingLaneStatus._2.getCongestion; SNLanesCount += 1
            }
        }
        // At the beginning, some lane statuses might not have been produced yet.
        if (EWLanesCount != 0) congestionEW /= EWLanesCount
        if (SNLanesCount != 0) congestionSN /= SNLanesCount
        congestionEW = Math.round(congestionEW * 10) / 10.0
        congestionSN = Math.round(congestionSN * 10) / 10.0
        if (congestionEW > 0 || congestionSN > 0) {
            printLog("Congestion in incoming lanes for semaphore " + self.toString() + "is: EW " +
                congestionEW * 100 + "%," + " SN " + congestionSN * 100 + "%. Current strategy is " +
                currentTrafficFlowStrategy.getClass, LoggerPriority.LOW)
        }
        (congestionEW, congestionSN)
    }

    /**
      * Calculate the traffic incoming ratio (congestion/# lanes) using the information provided by the adjacent semaphores
      *
      * @return (ratioEW, ratioSN) a tuple with the two congestion ratios
      */
    private def calculateIncomingTrafficFromOtherSemaphores(): (Double, Double) = {
        var congestionEW = 0.0
        var EWLanesCount = 0
        var congestionSN = 0.0
        var SNLanesCount = 0
        for (adjacentSemaphore <- adjacentSemaphoreStatus.values) {
            var semaphoreDirectionComparedToThis: Direction = Direction.NORTH // I need to init this and doesn't matter
            // Get direction
            for (outgoingLane <- adjacentSemaphore.getOutgoingLanes) {
                if (incomingLanes.contains(outgoingLane._1))
                    semaphoreDirectionComparedToThis = incomingLanes(outgoingLane._1)
            }
            // Get congestion ratio and number of lanes
            for (outgoingLane <- adjacentSemaphore.getOutgoingLanes) {
                if (!incomingLanes.contains(outgoingLane._1)) {
                    if (semaphoreDirectionComparedToThis == Direction.EAST ||
                        semaphoreDirectionComparedToThis == Direction.WEST) {
                        congestionEW += outgoingLane._2.getCongestion
                        EWLanesCount += 1
                    } else {
                        congestionSN += outgoingLane._2.getCongestion
                        SNLanesCount += 1
                    }
                }
            }
        }
        (congestionEW / EWLanesCount, congestionSN / SNLanesCount)
    }


    /**
      * Update the semaphore strategy according to the congestion levels
      *
      * @param congestionEW congestion on incoming lanes East/West
      * @param congestionSN congestion on incoming lanes South/North
      */
    private def updateSemaphoreStrategy(congestionEW: Double, congestionSN: Double): Unit = {
        if (congestionEW < Semaphore.CONGESTION_THRESHOLD && congestionSN < Semaphore.CONGESTION_THRESHOLD) {
            updateToDefaultStrategy()
        } else if (congestionEW >= Semaphore.CONGESTION_THRESHOLD && congestionSN >= Semaphore.CONGESTION_THRESHOLD) {
            for (semaphore <- adjacentSemaphores)
                semaphore ! new TrafficDetailsRequestMessage
            updateToCollaborativeStrategy()
        } else {
            updateToAutonomousStrategy(congestionEW, congestionSN)
            previousEWCongestionRate = congestionEW
            previousSNCongestionRate = congestionSN
        }
        currentTrafficFlowStrategy.setCurrentState(currentState)
    }

    /**
      * Update semaphore strategy to default strategy
      */
    private def updateToDefaultStrategy(): Unit = {
        if (!currentTrafficFlowStrategy.isInstanceOf[DefaultSemaphore]) {
            currentTrafficFlowStrategy = currentTrafficFlowStrategy.changeToDefault()
            currentTrafficFlowStrategy.updateTimers()
            printLogNewStrategy()
            updateSemaphoreStrategyInGui(utilities.enumerations.SemaphoreStrategy.DEFAULT)
        }
    }

    /**
      * Update semaphore strategy to collaborative strategy
      */
    private def updateToCollaborativeStrategy(): Unit = {
        if (!currentTrafficFlowStrategy.isInstanceOf[CollaborativeSemaphore]) {
            currentTrafficFlowStrategy = currentTrafficFlowStrategy.changeToCollaborative()
            printLogNewStrategy()
            updateSemaphoreStrategyInGui(utilities.enumerations.SemaphoreStrategy.COLLABORATIVE)
        }
    }

    /**
      * Update semaphore strategy to autonomous strategy
      *
      * @param congestionEW the degree of congestion of the lanes from East and West
      * @param congestionSN the degree of congestion of the lanes from South and North
      */
    private def updateToAutonomousStrategy(congestionEW: Double, congestionSN: Double): Unit = {
        if (!currentTrafficFlowStrategy.isInstanceOf[AutonomousSemaphore] ||
            changeSemaphoreCongestion(congestionEW, congestionSN)) {
            if (!currentTrafficFlowStrategy.isInstanceOf[AutonomousSemaphore]) {
                currentTrafficFlowStrategy = currentTrafficFlowStrategy.changeToAutonomous()
                printLogNewStrategy()
                updateSemaphoreStrategyInGui(utilities.enumerations.SemaphoreStrategy.AUTONOMOUS)
            }
            currentTrafficFlowStrategy.asInstanceOf[AutonomousSemaphore].setCongestion(congestionEW, congestionSN)
            currentTrafficFlowStrategy.updateTimers()
        }
    }

    /**
      * Print in the log the new semaphore's strategy
      */
    private def printLogNewStrategy() = {
        printLog("Semaphore " + self.path.name + " strategy is updated to " +
            currentTrafficFlowStrategy.getClass.getSimpleName, LoggerPriority.HIGH)
    }
}