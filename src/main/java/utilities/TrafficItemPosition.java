package utilities;

/**
 * Class which represent a TrafficItem position in the City
 *
 * @author Chiara Ceccarini
 * @author Alberto Mulazzani
 */
public class TrafficItemPosition {
    private int x;
    private int y;

    /**
     * @param x the x position of the TrafficItem in the matrix
     * @param y the y position if the TrafficItem in the matrix
     */
    public TrafficItemPosition(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
