package utilities.filecreator;

import gui.StatisticsView;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static utilities.filecreator.XmlFileCreatorConstants.XML;
import static utilities.filecreator.XmlFileCreatorConstants.XML_LABEL;

/**
 * Implementation of TextFilePlotter that allows to plot a dataset starting from an existing XML file
 *
 * @author Alessio Addimando
 */
public class StatisticsXmlTextFilePlotter implements TextFilePlotter<XYSeriesCollection, String> {

    private File currentFile = null;
    private final Map<Double, Integer> statisticsToPlot = new HashMap<>();

    public StatisticsXmlTextFilePlotter() {
        JFileChooser fileChooser = new JFileChooser(System.getProperty(XmlFileCreatorConstants.USER_HOME));
        FileNameExtensionFilter xmlFilter = new FileNameExtensionFilter(XML_LABEL, XML, XML);
        fileChooser.setAcceptAllFileFilterUsed(true);
        fileChooser.addChoosableFileFilter(xmlFilter);
        int returnVal = fileChooser.showOpenDialog(new JFrame());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            this.currentFile = fileChooser.getSelectedFile();
            processTextDocument();
        }
    }

    /**
     * Sets the document from an existing file and processes it
     */
    @Override
    public void processTextDocument() {

        Document documentToRead = createDocument();
        if (documentToRead != null) {
            getLaneInfo(documentToRead);
            plotDatasetFromFile(createDatasetToPlot(), this.currentFile.getName());
        }
    }

    /**
     * @param dataset    a XYSeriesCollection to plot on a chart
     * @param fileToPlot the name of the XML file plotted, useful for the main label in the chart's frame
     */
    @Override
    public void plotDatasetFromFile(XYSeriesCollection dataset, String fileToPlot) {
        new StatisticsView(dataset, fileToPlot).setVisible(true);
    }

    /**
     * Method that creates a document
     *
     * @return the document to process in order to extract needed information
     */
    public Document createDocument() {
        DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory
                .newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = docBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        Document doc = null;
        try {
            assert docBuilder != null;
            doc = docBuilder.parse(this.currentFile);
            doc.getDocumentElement().normalize();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e1) {
            System.out.println("Operation aborted by user.");
        } catch (SAXException e2) {
            System.out.println("Unknown file format. Please use our XML files.");
        }
        return doc;
    }

    /**
     * Traverse an XML document, getting the information on each lane in
     * the list. The document is assumed to contain a list of "lane" nodes,
     * each of which has a "average-traffic" element. It extracts the
     * values associated with each of those tags.
     *
     * @param document the XML document containing the lane's info
     */
    private void getLaneInfo(Document document) {
        NodeList nodeList = document.getElementsByTagName(XmlFileCreatorConstants.LANE);
        for (int i = 0; i < nodeList.getLength(); i++) {
            getAverageTrafficInfo(nodeList.item(i));
        }
    }

    /**
     * Extracts the information about each lane and its traffic average percentage
     *
     * @param node an XML node encapsulating traffic info
     */
    private void getAverageTrafficInfo(Node node) {
        if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element lane = (Element) node;
            addTrafficDataToMap(lane, XmlFileCreatorConstants.AVERAGE_TRAFFIC);
        }
    }

    /**
     * Adds row data in a generic map
     *
     * @param lane the XML Element containing a lane
     */
    private void addTrafficDataToMap(Element lane, final String component) {

        NodeList elements = lane.getElementsByTagName(component);
        Element firstElement = (Element) elements.item(0);
        NodeList children = firstElement.getChildNodes();
        if (this.statisticsToPlot.containsKey(Double.parseDouble(children.item(0).getNodeValue()))) {
            this.statisticsToPlot.replace(Double.parseDouble(children.item(0).getNodeValue()),
                    this.statisticsToPlot.get(Double.parseDouble(children.item(0).getNodeValue())) + 1);
        } else {
            this.statisticsToPlot.put(Double.parseDouble(children.item(0).getNodeValue()), 1);
        }
    }

    /**
     * Method that creates a dataset to plot
     *
     * @return starting from a map creates a dataset in order to plot it on a generic chart
     */
    private XYSeriesCollection createDatasetToPlot() {
        XYSeries trafficData = new XYSeries("Average Traffic Statistics");
        for (Map.Entry<Double, Integer> currentValue : this.statisticsToPlot.entrySet()) {
            trafficData.add(currentValue.getKey(), currentValue.getValue());
        }
        return new XYSeriesCollection(trafficData);
    }


}