package messages.init;

import akka.actor.ActorRef;

/**
 * Message sent from the MapForm to the City to set the number of semaphores and the number of vehicles
 * and pass the references at the actual Viewer Actor
 *
 * @author Enrico Cagnazzo
 */

public final class InitCityMessage {
    private int numberOfSemaphores;
    private int numberOfVehicles;
    private ActorRef viewer;

    /**
     * @param numberOfSemaphores the number of the Semaphores in the City
     * @param numberOfVehicles   the number of Vehicles in the City
     * @param viewer             the reference to the viewer Actor
     */
    public InitCityMessage(int numberOfSemaphores, int numberOfVehicles, ActorRef viewer) {
        this.numberOfSemaphores = numberOfSemaphores;
        this.numberOfVehicles = numberOfVehicles;
        this.viewer = viewer;
    }

    public int getNumberOfSemaphores() {
        return numberOfSemaphores;
    }

    public int getNumberOfVehicles() {
        return numberOfVehicles;
    }

    public ActorRef getViewer() {
        return viewer;
    }
}
