package entities

import java.util

import akka.actor.ActorRef
import messages.DestinationReachedMessage
import messages.gui._
import messages.init.InitViewMessage
import utilities.enumerations.{Direction, SemaphoreDirection}
import utilities.{LaneStatus, enumerations}

/**
  * Extends AbstractActorWithLogger and adds the mapViewer that are needed through the whole System
  *
  * @author Alberto Mulazzani
  * @author Michele Campobasso
  */
trait AbstractActorWithUpuautUtils extends AbstractActorWithLogger {

    var viewer: ActorRef = _

    /**
      * Updates the representation of the Semaphore into the GUI according to the current Strategy
      *
      * @param strategy Current Semaphore strategy
      */
    protected def updateSemaphoreStrategyInGui(strategy: enumerations.SemaphoreStrategy): Unit =
        if (viewer != null) viewer ! new ChangeSemaphoreStrategyGuiMessage(self, strategy)

    /**
      * Updates the lights of the Semaphore into the GUI according to the current Semaphore Status
      *
      * @param status Current Semaphore status
      */
    protected def updateSemaphoreLightsInGui(status: SemaphoreDirection): Unit =
        if (viewer != null) viewer ! new ChangeSemaphoreLightsGuiMessage(self, status)

    /**
      * Updates the color of the lane according to its congestion degree
      *
      * @param status Current Lane status
      */
    protected def updateLaneCongestionInGui(status: LaneStatus): Unit =
        if (viewer != null) viewer ! new ChangeLaneGuiMessage(self, status)

    /**
      * Initializes the View passing Lanes, Semaphores, Statistician and the number of Vehicles of a City
      *
      * @param lanes        The Lanes of the City
      * @param semaphores   The Semaphores of the City
      * @param statistician The Statistician Actor
      * @param nVehicles    The number of Vehicles of a City
      */
    protected def initView(lanes: util.ArrayList[util.ArrayList[util.Map[Direction, ActorRef]]],
                           semaphores: util.ArrayList[util.ArrayList[ActorRef]],
                           statistician: ActorRef, nVehicles: Int): Unit =
        if (viewer != null) viewer ! new InitViewMessage(lanes, semaphores, statistician, nVehicles)

    /**
      * Updates the number of active Vehicles on GUI
      *
      * @param nVehicles number of Vehicles on GUI
      */
    protected def changeNumberOfVehiclesOnGui(nVehicles: Int): Unit =
        if (viewer != null) viewer ! new ChangeNumberOfVehiclesGuiMessage(nVehicles)

    /**
      * Updates the number of arrived Vehicles on GUI
      *
      * @param isStandardCity determines whether the city where the Vehicle arrivesis a Standard or an Upuaut one
      */
    protected def changeNumberOfArrivedVehiclesOnGui(isStandardCity: Boolean): Unit =
        if (viewer != null) viewer ! new DestinationReachedMessage(isStandardCity)

}
