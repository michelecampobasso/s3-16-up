package utilities.enumerations;

/**
 * Enum that represents the possible states of a Semaphore Actor.
 *
 * @author Chiara Ceccarini
 */
public enum SemaphoreColor {
    GREEN,
    RED;

    @Override
    public String toString() {
        return this.name();
    }
}
