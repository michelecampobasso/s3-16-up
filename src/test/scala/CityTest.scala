import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import entities.City
import messages._
import messages.init.InitCityMessage
import messages.utility.StartMessage
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}


/**
  * @author Chiara Ceccarini
  * @author Michele Campobasso
  * @author Alberto Mulazzani
  */
class CityTest() extends TestKit(ActorSystem("MySpec"))
    with ImplicitSender
    with WordSpecLike
    with BeforeAndAfterAll
    with Matchers {

    private val cityActor = system.actorOf(Props[City])

    override def afterAll {
        TestKit.shutdownActorSystem(system)
    }

    override def beforeAll: Unit = {
        cityActor ! new InitCityMessage(6, 1000, null)
        cityActor ! new StartMessage()
    }

    "A City actor" must {

        "send back city map" in {
            cityActor ! new MapRequestMessage
            expectMsgClass(classOf[MapResponseMessage].getSuperclass)
        }

        "not reply after destination reached" in {
            cityActor ! new DestinationReachedMessage
            expectNoMsg()
        }
    }
}



