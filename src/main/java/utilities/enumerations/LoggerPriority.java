package utilities.enumerations;

/**
 * Enum that represents the possible priorities of a PrintLogMessage sent to the Logger Actor.
 *
 * @author Alberto Mulazzani
 */
public enum LoggerPriority {
    DISABLED,
    HIGH,
    LOW;

    @Override
    public String toString() {
        return this.name();
    }
}
