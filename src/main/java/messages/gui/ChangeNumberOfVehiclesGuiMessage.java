package messages.gui;

/**
 * A message sent from the City Actor to the MapViewer to update the number of vehicles that are in the streets
 *
 * @author Enrico Cagnazzo
 */
public final class ChangeNumberOfVehiclesGuiMessage {
    private int numberOfVehicles;

    /**
     * @param numberOfVehicles the number of vehicles in the City
     */
    public ChangeNumberOfVehiclesGuiMessage(int numberOfVehicles) {
        this.numberOfVehicles = numberOfVehicles;
    }

    public int getNumberOfVehicles() {
        return numberOfVehicles;
    }
}
