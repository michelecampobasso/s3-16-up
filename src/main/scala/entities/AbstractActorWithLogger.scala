package entities

import akka.actor.{AbstractActor, ActorRef}
import messages.utility.PrintLogMessage
import utilities.enumerations.LoggerPriority

/**
  * Extends AbstractActor and adds the log where is needed through the whole System
  *
  * @author Michele Campobasso
  */
trait AbstractActorWithLogger extends AbstractActor {

    var loggerRef: ActorRef = _

    /**
      * Sends a message to the Logger Actor with the name of the sender, the message
      * to log and the priority
      *
      * @param message  The message to be logged
      * @param priority The priority of the message
      */
    protected def printLog(message: String, priority: LoggerPriority): Unit =
        loggerRef ! new PrintLogMessage(self.path.name, message, priority)

}
