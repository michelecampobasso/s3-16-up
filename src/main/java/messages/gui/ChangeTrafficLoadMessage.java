package messages.gui;

import utilities.enumerations.TrafficLoadState;

/**
 * Message sent from the Load Controller Actor to the MapViewer to change the load's state of the City
 *
 * @author Alberto Mulazzani
 */
public final class ChangeTrafficLoadMessage {
    private TrafficLoadState state;

    /**
     * @param state the new state of the City's load
     */
    public ChangeTrafficLoadMessage(TrafficLoadState state) {
        this.state = state;
    }

    public TrafficLoadState getState() {
        return state;
    }
}
