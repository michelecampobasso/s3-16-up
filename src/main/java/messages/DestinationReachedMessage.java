package messages;

/**
 * Message sent from the Vehicle Actor to the City Actor to notify his arrival.
 *
 * @author Chiara Ceccarini
 * @author Michele Campobasso
 */
public final class DestinationReachedMessage {

    private boolean isStandardCity;
    private int travelTime;

    public DestinationReachedMessage() {
    }

    /**
     * @param isStandardCity says if a Vehicle is arrived to his destination from a Standard or Upuaut City.
     */
    public DestinationReachedMessage(boolean isStandardCity) {
        this.isStandardCity = isStandardCity;
    }

    /**
     * @param travelTime represents the time of travel in seconds of a Vehicle
     */
    public DestinationReachedMessage(int travelTime) {
        this.travelTime = travelTime;
    }

    public boolean isStandardCity() {
        return isStandardCity;
    }

    public int getTravelTime() {
        return travelTime;
    }
}
