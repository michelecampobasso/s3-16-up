package utilities.enumerations;

/**
 * Enum that represent the Semaphores' strategies
 *
 * @author Alessio Addimando
 * @author Alberto Mulazzani
 */
public enum SemaphoreStrategy {
    DEFAULT,
    AUTONOMOUS,
    COLLABORATIVE;

    @Override
    public String toString() {
        return this.name();

    }
}
