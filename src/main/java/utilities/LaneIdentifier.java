package utilities;

import akka.actor.ActorRef;

/**
 * Allows to identify a particular lane between two semaphore
 *
 * @author Alessio Addimando
 */
public class LaneIdentifier {

    private ActorRef semaphoreFrom;
    private ActorRef semaphoreTo;

    /**
     * @param semaphoreFrom the origin Semaphore of the Lane
     * @param semaphoreTo   the destination Semaphore of the Lane
     */
    public LaneIdentifier(ActorRef semaphoreFrom, ActorRef semaphoreTo) {
        this.semaphoreFrom = semaphoreFrom;
        this.semaphoreTo = semaphoreTo;
    }

    public ActorRef getSemaphoreFrom() {
        return semaphoreFrom;
    }

    public ActorRef getSemaphoreTo() {
        return semaphoreTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LaneIdentifier that = (LaneIdentifier) o;

        return semaphoreFrom.equals(that.semaphoreFrom) && semaphoreTo.equals(that.semaphoreTo);
    }

    @Override
    public int hashCode() {
        int result = semaphoreFrom.hashCode();
        result = 31 * result + semaphoreTo.hashCode();
        return result;
    }

}
