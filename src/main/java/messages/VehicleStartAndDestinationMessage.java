package messages;

import utilities.LanePosition;

/**
 * Message sent from the vehicle to the city after the creation of a new vehicle. The message contains the
 * start position and the destination
 *
 * @author Enrico Cagnazzo
 */
public final class VehicleStartAndDestinationMessage {
    private LanePosition startVehiclePosition;
    private LanePosition destinationVehiclePosition;

    public VehicleStartAndDestinationMessage(LanePosition startVehiclePosition, LanePosition destinationVehiclePosition) {
        this.startVehiclePosition = startVehiclePosition;
        this.destinationVehiclePosition = destinationVehiclePosition;
    }

    public LanePosition getStartVehiclePosition() {
        return startVehiclePosition;
    }

    public LanePosition getDestinationVehiclePosition() {
        return destinationVehiclePosition;
    }
}
