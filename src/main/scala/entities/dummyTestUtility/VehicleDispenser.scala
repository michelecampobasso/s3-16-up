package entities.dummyTestUtility

import java.util

import akka.actor.{ActorRef, Props}
import entities.{AbstractActorWithLogger, Vehicle}
import messages._
import messages.bind.BindLoggerMessage
import messages.init.{InitVehicleDispenserMessage, InitVehicleMessage}
import utilities.LanePosition
import utilities.enumerations.{Direction, LoggerPriority}


/**
  * VehicleDispenser Actor. It creates a number of Vehicle Actors.
  *
  * @author Enrico Cagnazzo
  * @author Michele Campobasso
  */
class VehicleDispenser extends AbstractActorWithLogger {

    private var cityActor: ActorRef = _

    private var cityLanes: util.ArrayList[util.ArrayList[util.Map[Direction, ActorRef]]] = _
    private var numberOfCars: Int = 0
    private val randomGenerator = scala.util.Random

    private val bigTrafficSourcesCoordinates: util.ArrayList[LanePosition] = new util.ArrayList[LanePosition]()

    /**
      * A Vehicle Actor can receive and handle these messages:<br>
      *
      * BindLoggerMessage<br>
      * InitVehicleDispenserMessage<br>
      * AddVehicleMessage<br>
      */
    override def receive = {

        case message: BindLoggerMessage => loggerRef = message.getLoggerRef

        case message: InitVehicleDispenserMessage =>
            cityLanes = message.getLanes
            cityActor = context.sender()
            chooseTrafficSources()

        case message: AddVehicleMessage =>
            for (_ <- 0 until message.getVehiclesNumber) {
                createVehicle(numberOfCars, chooseStartPosition(), chooseDestination(), true)
                numberOfCars = numberOfCars + 1
            }

        case message: AddVehicleWithPositionMessage =>
            createVehicle(numberOfCars, message.getStartVehiclePosition, message.getDestinationVehiclePosition, false)
            numberOfCars = numberOfCars + 1
    }

    /**
      * Choose a random lane in the city as start position
      *
      * @return the starting lane
      */
    private def chooseStartPosition(): LanePosition = {
        val dice: Int = randomGenerator.nextInt(10)
        val lanePosition = bigTrafficSourcesCoordinates.get(randomGenerator.nextInt(bigTrafficSourcesCoordinates.size()))
        lanePosition.setDirection(Direction.values().apply(randomGenerator.nextInt(4)))

        if (dice < 7) {
            var aroundX = lanePosition.getX - 1 + randomGenerator.nextInt(3)
            var aroundY = lanePosition.getY - 1 + randomGenerator.nextInt(3)
            while (aroundX < 0 || aroundX >= cityLanes.size || aroundY < 0 || aroundY >= cityLanes.get(0).size) {
                aroundX = lanePosition.getX - 1 + randomGenerator.nextInt(3)
                aroundY = lanePosition.getY - 1 + randomGenerator.nextInt(3)
            }
            if (cityLanes.get(aroundX).get(aroundY).containsKey(lanePosition.getDirection))
                new LanePosition(aroundX, aroundY, lanePosition.getDirection)
            else new LanePosition(aroundX, aroundY, Direction.oppositeDirection(lanePosition.getDirection))
        }
        else {
            val x: Int = randomGenerator.nextInt(cityLanes.size)
            val y: Int = randomGenerator.nextInt(cityLanes.get(x).size)
            val direction: Direction = cityLanes.get(x).get(y).keySet.toArray(
                new Array[Direction](0))(randomGenerator.nextInt(cityLanes.get(x).get(y).size()))
            new LanePosition(x, y, direction)
        }
    }

    /**
      * Choose a random lane in the city and its opposite as destinations (x to y and y to x)
      *
      * @return a list of two lanes
      */
    private def chooseDestination(): LanePosition = {
        val x: Int = randomGenerator.nextInt(cityLanes.size)
        val y: Int = randomGenerator.nextInt(cityLanes.get(x).size)
        val direction: Direction = cityLanes.get(x).get(y).keySet.toArray(
            new Array[Direction](0))(randomGenerator.nextInt(cityLanes.get(x).get(y).size()))
        new LanePosition(x, y, direction)
    }

    /**
      * Create and initialize a new vehicle
      *
      * @param i the vehicle's number
      */
    private def createVehicle(i: Int, startVehiclePosition: LanePosition, destinationVehiclePosition: LanePosition, forwardVehicleToStandardCity: Boolean): Unit = {
        val vehicle: ActorRef = context.actorOf(Props[Vehicle], "Vehicle" + i)
        vehicle ! new BindLoggerMessage(loggerRef)
        val startLane = cityLanes.get(startVehiclePosition.getX).get(startVehiclePosition.getY).get(startVehiclePosition.getDirection)
        val destination: util.List[ActorRef] = new util.ArrayList[ActorRef]()
        destination.add(cityLanes.get(destinationVehiclePosition.getX).get(destinationVehiclePosition.getY).get(destinationVehiclePosition.getDirection))
        destinationVehiclePosition.getDirection match {
            case Direction.NORTH => destination.add(cityLanes.get(destinationVehiclePosition.getX - 1).get(destinationVehiclePosition.getY).get(Direction.SOUTH))
            case Direction.EAST => destination.add(cityLanes.get(destinationVehiclePosition.getX).get(destinationVehiclePosition.getY + 1).get(Direction.WEST))
            case Direction.SOUTH => destination.add(cityLanes.get(destinationVehiclePosition.getX + 1).get(destinationVehiclePosition.getY).get(Direction.NORTH))
            case Direction.WEST => destination.add(cityLanes.get(destinationVehiclePosition.getX).get(destinationVehiclePosition.getY - 1).get(Direction.EAST))
        }
        vehicle ! new InitVehicleMessage(startLane, destination, cityActor)
        if (forwardVehicleToStandardCity)
            cityActor ! new VehicleStartAndDestinationMessage(startVehiclePosition, destinationVehiclePosition)
        printLog("created", LoggerPriority.LOW)
    }

    /**
      * Creates a variable number of spawn points for vehicles proportional to
      * City's size.
      */
    private def chooseTrafficSources(): Unit = {
        for (_ <- 0 until cityLanes.size() / 3) {
            var x: Int = randomGenerator.nextInt(cityLanes.size)
            var y: Int = randomGenerator.nextInt(cityLanes.get(x).size)
            val direction: Direction = cityLanes.get(x).get(y).keySet.toArray(
                new Array[Direction](0))(randomGenerator.nextInt(cityLanes.get(x).get(y).size()))

            var isDistant = false
            while (!isDistant) {
                isDistant = true
                x = randomGenerator.nextInt(cityLanes.size)
                y = randomGenerator.nextInt(cityLanes.get(x).size)
                for (i <- 0 until bigTrafficSourcesCoordinates.size()) {
                    if (distanceBetweenTwoPoints((x, y),
                        (bigTrafficSourcesCoordinates.get(i).getX, bigTrafficSourcesCoordinates.get(i).getY)) < cityLanes.size() / 3 + 1)
                        isDistant = false
                }
            }
            bigTrafficSourcesCoordinates.add(new LanePosition(x, y, direction))
        }
    }

    /**
      * Calculates distance between two points.
      *
      * @param pointA first point
      * @param pointB second point
      * @return distance between points
      */
    private def distanceBetweenTwoPoints(pointA: (Int, Int), pointB: (Int, Int)): Double = {
        Math.sqrt(Math.pow(pointA._1 - pointB._1, 2) +
            Math.pow(pointA._2 - pointB._2, 2))
    }
}