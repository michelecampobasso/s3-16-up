package entities

import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, Cancellable, Scheduler}
import messages.ChangeStatusMessage
import utilities.enumerations.SemaphoreDirection

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.FiniteDuration

/**
  * Create the different strategies for the semaphore (default, autonomous and collaborative).
  * For each strategy there is a class that extends the trait SemaphoreStrategy.
  *
  * @author Enrico Cagnazzo
  */
object SemaphoreStrategy {

    /**
      * Creates a semaphore with the default strategy
      *
      * @param scheduler the scheduler used to create timers
      * @param semaphore the reference at the corresponding actor semaphore
      * @return default semaphore
      */
    def createDefault(scheduler: Scheduler, semaphore: ActorRef): DefaultSemaphore = {
        val defaultSemaphore = new DefaultSemaphore
        defaultSemaphore.setScheduler(scheduler)
        defaultSemaphore.setSemaphore(semaphore)
        val defaultStrategy = scheduler.schedule(
            initialDelay = FiniteDuration.apply(Semaphore.SINGLE_TIME_CHANGE_LANTERN_COLOR, TimeUnit.MILLISECONDS),
            interval = FiniteDuration.apply(Semaphore.SINGLE_TIME_CHANGE_LANTERN_COLOR, TimeUnit.MILLISECONDS)) {
            semaphore ! new ChangeStatusMessage()
        }
        defaultSemaphore.setTimers(defaultStrategy, null, null)
        defaultSemaphore
    }
}

/**
  * It is the strategy that a Semaphore can implement
  */
sealed trait SemaphoreStrategy {

    var changeLanternColorTimerDefault: Cancellable = _
    var changeLanternColorTimerLong: Cancellable = _
    var changeLanternColorTimerShort: Cancellable = _

    var currentState: SemaphoreDirection = _

    var scheduler: Scheduler = _

    var semaphore: ActorRef = _

    /**
      * Method that updates the timers
      */
    def updateTimers(): Unit

    protected def setTimers(default: Cancellable, long: Cancellable, short: Cancellable): Unit = {
        changeLanternColorTimerDefault = default
        changeLanternColorTimerLong = long
        changeLanternColorTimerShort = short
    }

    protected def setScheduler(scheduler: Scheduler): Unit = this.scheduler = scheduler

    protected def setSemaphore(semaphore: ActorRef): Unit = this.semaphore = semaphore

    def setCurrentState(state: SemaphoreDirection): Unit = currentState = state

    /**
      * Cancels timers. This method is called before reassigning new timers.
      */
    protected def cancelTimers(): Unit = {
        if (changeLanternColorTimerDefault != null) changeLanternColorTimerDefault.cancel
        if (changeLanternColorTimerLong != null) changeLanternColorTimerLong.cancel
        if (changeLanternColorTimerShort != null) changeLanternColorTimerShort.cancel
    }

    /**
      * Set the timers changeLanternColorTimerLong and changeLanternColorTimerShort used by the strategies
      * autonomous and collaboratives.
      *
      * @param switchLongShortTime tell if the lantern color should change soon or late.
      */
    protected def setUpTimersLongShort(switchLongShortTime: Boolean): Unit = {
        changeLanternColorTimerLong = scheduler.schedule(
            initialDelay = FiniteDuration.apply(if (switchLongShortTime) Semaphore.LONG_TIME_CHANGE_LANTERN_COLOR
            else Semaphore.SHORT_TIME_CHANGE_LANTERN_COLOR,
                TimeUnit.MILLISECONDS),
            interval = FiniteDuration.apply(Semaphore.DOUBLE_TIME_CHANGE_LANTERN_COLOR, TimeUnit.MILLISECONDS)) {
            semaphore ! new ChangeStatusMessage()
        }
        changeLanternColorTimerShort = scheduler.schedule(
            initialDelay = FiniteDuration.apply(Semaphore.DOUBLE_TIME_CHANGE_LANTERN_COLOR, TimeUnit.MILLISECONDS),
            interval = FiniteDuration.apply(Semaphore.DOUBLE_TIME_CHANGE_LANTERN_COLOR, TimeUnit.MILLISECONDS)) {
            semaphore ! new ChangeStatusMessage()
        }
    }

    /**
      * Change the actual strategy to default.
      *
      * @return A new strategy with the same fields (scheduler, semaphore and timers) of the previous one.
      */
    def changeToDefault(): DefaultSemaphore = {
        val newStrategy = new DefaultSemaphore
        newStrategy.setScheduler(scheduler)
        newStrategy.setSemaphore(semaphore)
        newStrategy.setTimers(changeLanternColorTimerDefault, changeLanternColorTimerLong, changeLanternColorTimerShort)
        newStrategy
    }

    /**
      * Change the actual strategy to autonomous.
      *
      * @return A new strategy with the same fields (scheduler, semaphore and timers) of the previous one.
      */
    def changeToAutonomous(): AutonomousSemaphore = {
        val newStrategy = new AutonomousSemaphore
        newStrategy.setScheduler(scheduler)
        newStrategy.setSemaphore(semaphore)
        newStrategy.setTimers(changeLanternColorTimerDefault, changeLanternColorTimerLong, changeLanternColorTimerShort)
        newStrategy
    }

    /**
      * Change the actual strategy to collaborative.
      *
      * @return A new strategy with the same fields (scheduler, semaphore and timers) of the previous one.
      */
    def changeToCollaborative(): CollaborativeSemaphore = {
        val newStrategy = new CollaborativeSemaphore
        newStrategy.setScheduler(scheduler)
        newStrategy.setSemaphore(semaphore)
        newStrategy.setTimers(changeLanternColorTimerDefault, changeLanternColorTimerLong, changeLanternColorTimerShort)
        newStrategy
    }

}

/**
  * It is the Default behaviour of the Semaphore
  */
class DefaultSemaphore() extends SemaphoreStrategy {

    /**
      * Update the change lantern color timers in the Default case.
      */
    override def updateTimers(): Unit = {
        cancelTimers()
        changeLanternColorTimerDefault = scheduler.schedule(
            initialDelay = FiniteDuration.apply(Semaphore.SINGLE_TIME_CHANGE_LANTERN_COLOR, TimeUnit.MILLISECONDS),
            interval = FiniteDuration.apply(Semaphore.SINGLE_TIME_CHANGE_LANTERN_COLOR, TimeUnit.MILLISECONDS)) {
            semaphore ! new ChangeStatusMessage()
        }
    }
}

/**
  * It is the Autonomous behaviour of the Semaphore
  */
class AutonomousSemaphore() extends SemaphoreStrategy {

    private var congestionEW: Double = 0.0
    private var congestionSN: Double = 0.0

    def setCongestion(congestionEW: Double, congestionSN: Double): Unit = {
        this.congestionEW = congestionEW
        this.congestionSN = congestionSN
    }

    /**
      * Update the change lantern color timers according the congestion levels of adjacent lanes
      * in the Autonomous case.
      */
    override def updateTimers(): Unit = {
        cancelTimers()
        val switchLongShortTime =
            (congestionEW >= Semaphore.CONGESTION_THRESHOLD && currentState == SemaphoreDirection.WEST_EAST) ||
                (congestionSN >= Semaphore.CONGESTION_THRESHOLD && currentState == SemaphoreDirection.NORTH_SOUTH)
        setUpTimersLongShort(switchLongShortTime)
    }
}

/**
  * It is the Collaborative behaviour of the Semaphore
  */
class CollaborativeSemaphore() extends SemaphoreStrategy {
    private var ratioEW: Double = 0.0
    private var ratioSN: Double = 0.0

    def setRatios(ratioEW: Double, ratioSN: Double): Unit = {
        this.ratioEW = ratioEW
        this.ratioSN = ratioSN
    }

    /**
      * Update the change lantern color timers according the congestion ratios provided by the adjacent semaphores
      * in the Collaborative case.
      */
    override def updateTimers(): Unit = {
        cancelTimers()
        val switchLongShortTime = (ratioEW > ratioSN && (currentState == SemaphoreDirection.WEST_EAST)) ||
            (ratioSN > ratioEW && (currentState == SemaphoreDirection.NORTH_SOUTH))
        setUpTimersLongShort(switchLongShortTime)
    }
}

