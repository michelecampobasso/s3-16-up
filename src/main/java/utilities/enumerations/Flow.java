package utilities.enumerations;

/**
 * Enum that represents the flow of a Lane Actor in a Semaphore Actor.
 *
 * @author Alessio Addimando
 * @author Alberto Mulazzani
 */
public enum Flow {
    INCOMING,
    OUTGOING;

    @Override
    public String toString() {
        return this.name();
    }
}
