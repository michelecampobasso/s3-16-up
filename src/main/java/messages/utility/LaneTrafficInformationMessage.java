package messages.utility;

import utilities.LaneIdentifier;

/**
 * Message sent from the Lane to the Statistician Actor with its own traffic's information
 *
 * @author Alessio Addimando
 */
public final class LaneTrafficInformationMessage {
    private LaneIdentifier laneIdentifier;
    private Double currentTrafficPercentage;

    /**
     * @param laneIdentifier           the identifier of the Lane
     * @param currentTrafficPercentage the current percentage of the traffic in that Lane
     */
    public LaneTrafficInformationMessage(LaneIdentifier laneIdentifier, Double currentTrafficPercentage) {
        this.laneIdentifier = laneIdentifier;
        this.currentTrafficPercentage = currentTrafficPercentage;
    }

    public LaneIdentifier getLaneIdentifier() {
        return laneIdentifier;
    }

    public Double getCurrentTrafficPercentage() {
        return currentTrafficPercentage;
    }
}
