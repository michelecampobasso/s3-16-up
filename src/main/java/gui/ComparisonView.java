package gui;

import javax.swing.*;
import java.awt.*;

/**
 * Implementation of the comparison mode's GUI
 *
 * @author Michele Campobasso
 * @author Chiara Ceccarini
 */
public class ComparisonView extends JFrame {

    private static final Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
    private static final int ELEMENT_WIDTH = dimension.width / 3;
    private static final int PADDING = 10;

    private JProgressBar standardCityProgressBar;
    private JProgressBar upuautCityProgressBar;
    private JPanel upuautCityPanel;
    private JPanel standardCityPanel;

    private int numberOfTotalVehicles;

    private int numberOfArrivedVehiclesInStandardCity = 0;
    private int numberOfArrivedVehiclesInUpuautCity = 0;
    private int currentPercentageOfArrivedVehiclesInStandardCity = 0;
    private int currentPercentageOfArrivedVehiclesInUpuautCity = 0;

    private int averageTravelTimeInStandardCity = -1;
    private int averageTravelTimeInUpuautCity = -1;

    /**
     * @param numberOfTotalVehicles is the number of the vehicles that will run in the City.
     */
    public ComparisonView(int numberOfTotalVehicles) {

        this.numberOfTotalVehicles = numberOfTotalVehicles;

        EventQueue.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException |
                    InstantiationException |
                    IllegalAccessException |
                    UnsupportedLookAndFeelException ex) {
                ex.printStackTrace();
            }
            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            this.setTitle("Upuaut comparison mode");
            setLayout(new BorderLayout());

            Dimension progressBarDimension = new Dimension(ELEMENT_WIDTH, 20);
            drawUpuautCityProgress(progressBarDimension);
            drawStandardCityProgress(progressBarDimension);

            getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

            this.add(upuautCityPanel);
            this.add(standardCityPanel);

            final int x = dimension.width * 5 / 9;
            final int y = dimension.height * 7 / 30;
            this.setSize(new Dimension(x, y));
            setMinimumSize(new Dimension(x, y));

            pack();
            setResizable(true);
            setVisible(true);
        });
    }

    /**
     * Draws the Upuaut City ProgressBar.
     *
     * @param progressBarDimension the dimension of the ProgressBar
     */
    private void drawUpuautCityProgress(Dimension progressBarDimension) {
        upuautCityProgressBar = initProgressBar();

        upuautCityPanel = new JPanel();
        JPanel upuautCityProgressPanel = new JPanel();
        upuautCityProgressPanel.setLayout(new GridBagLayout());

        upuautCityProgressBar.setPreferredSize(progressBarDimension);
        upuautCityProgressPanel.add(upuautCityProgressBar);
        JLabel upuautCityLabel = new JLabel("Upuaut City");
        upuautCityLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

        SpringLayout upuautCityLayout = createUpuautLayout(upuautCityProgressPanel, upuautCityLabel);

        upuautCityPanel.setLayout(upuautCityLayout);

        upuautCityPanel.add(upuautCityLabel);
        upuautCityPanel.add(upuautCityProgressPanel);
    }

    /**
     * Draws the Standard City ProgressBar
     *
     * @param progressBarDimension the dimension of the ProgressBar
     */
    private void drawStandardCityProgress(Dimension progressBarDimension) {
        standardCityProgressBar = initProgressBar();

        JPanel standardCityProgressPanel = new JPanel();
        standardCityPanel = new JPanel();
        standardCityProgressPanel.setLayout(new GridBagLayout());

        standardCityProgressBar.setPreferredSize(progressBarDimension);
        standardCityProgressPanel.add(standardCityProgressBar);
        JLabel standardCityLabel = new JLabel("Standard City");
        standardCityLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        SpringLayout standardCityLayout = createStandardLayout(standardCityProgressPanel, standardCityLabel);

        standardCityPanel.setLayout(standardCityLayout);

        standardCityPanel.add(standardCityLabel);
        standardCityPanel.add(standardCityProgressPanel);
    }

    /**
     * Initializes a ProgressBar and sets the necessary values
     *
     * @return the ProgressBar object after the initialization.
     */
    private JProgressBar initProgressBar() {
        JProgressBar progressBar = new JProgressBar(0, 100);
        progressBar.setValue(0);
        progressBar.setStringPainted(true);
        return progressBar;
    }

    /**
     * Creates a SpringLayout for the Standard City ProgressBar and Label
     *
     * @param standardCityProgressPanel the Panel that contains the ProgressBar
     * @param standardCityLabel         the corresponding Label
     * @return a SpringLayout for the standardCity part
     */
    private SpringLayout createStandardLayout(JPanel standardCityProgressPanel, JLabel standardCityLabel) {
        SpringLayout standardCityLayout = new SpringLayout();
        standardCityLayout.putConstraint(SpringLayout.NORTH, standardCityProgressPanel, PADDING,
                SpringLayout.SOUTH, standardCityLabel);
        standardCityLayout.putConstraint(SpringLayout.NORTH, standardCityLabel, dimension.height / 30,
                SpringLayout.SOUTH, upuautCityPanel);
        standardCityLayout.putConstraint(SpringLayout.EAST, standardCityPanel, ELEMENT_WIDTH,
                SpringLayout.WEST, this);
        return standardCityLayout;
    }

    /**
     * Creates a SpringLayout for the Standard City ProgressBar and Label
     *
     * @param upuautCityProgressPanel the panel that contains the ProgressBar
     * @param upuautCityLabel         the corresponding Label
     * @return a SpringLayout for the upuautCity part
     */
    private SpringLayout createUpuautLayout(JPanel upuautCityProgressPanel, JLabel upuautCityLabel) {
        SpringLayout upuautCityLayout = new SpringLayout();
        upuautCityLayout.putConstraint(SpringLayout.NORTH, upuautCityProgressPanel, PADDING,
                SpringLayout.SOUTH, upuautCityLabel);
        upuautCityLayout.putConstraint(SpringLayout.EAST, upuautCityPanel, ELEMENT_WIDTH,
                SpringLayout.WEST, this);
        upuautCityLayout.putConstraint(SpringLayout.NORTH, upuautCityLabel, dimension.height / 100,
                SpringLayout.SOUTH, this);
        return upuautCityLayout;
    }

    /**
     * Updates the number of arrived vehicles in standard city and updates the percentage bar
     */
    public void updateNumberOfArrivedVehiclesInStandardCity() {
        currentPercentageOfArrivedVehiclesInStandardCity =
                100 * ++numberOfArrivedVehiclesInStandardCity / numberOfTotalVehicles;
        if (standardCityProgressBar.getValue() != currentPercentageOfArrivedVehiclesInStandardCity) {
            EventQueue.invokeLater(() -> this.standardCityProgressBar.setValue(
                    currentPercentageOfArrivedVehiclesInStandardCity));
        }
    }

    /**
     * Updates the number of arrived vehicles in Upuaut city and updates the percentage bar
     */
    public void updateNumberOfArrivedVehiclesInUpuautCity() {
        currentPercentageOfArrivedVehiclesInUpuautCity
                = 100 * ++numberOfArrivedVehiclesInUpuautCity / numberOfTotalVehicles;
        if (upuautCityProgressBar.getValue() != currentPercentageOfArrivedVehiclesInUpuautCity) {
            EventQueue.invokeLater(() -> this.upuautCityProgressBar.setValue(
                    currentPercentageOfArrivedVehiclesInUpuautCity));
        }
    }

    /**
     * Sets the average travel time of cars in standard city
     */
    public void setAverageTravelTimeInStandardCity(int averageTravelTimeInStandardCity) {
        this.averageTravelTimeInStandardCity = averageTravelTimeInStandardCity;
        if (this.averageTravelTimeInUpuautCity != -1) showSpeedupAlert();
    }

    /**
     * Sets the average travel time of cars in standard city
     */
    public void setAverageTravelTimeInUpuautCity(int averageTravelTimeInUpuautCity) {
        this.averageTravelTimeInUpuautCity = averageTravelTimeInUpuautCity;
        if (this.averageTravelTimeInStandardCity != -1) showSpeedupAlert();
    }

    /**
     * Shows an alert that calculates the speedup of Upuaut system over a standard city
     */
    private void showSpeedupAlert() {
        JOptionPane.showMessageDialog(this, "Speedup of Upuaut over a standard city is " +
                        (Math.round(((double) averageTravelTimeInStandardCity * 10 /
                                (double) averageTravelTimeInUpuautCity) * 10) - 100) + " %.",
                "Upuaut Speedup Results", JOptionPane.PLAIN_MESSAGE);
    }
}