package gui;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import messages.init.InitCitiesInComparisonViewerMessage;
import messages.init.InitCityInMapViewerMessage;
import messages.init.InitCityMessage;
import messages.utility.StartMessage;
import org.jfree.ui.RefineryUtilities;
import scala.concurrent.duration.Duration;
import utilities.filecreator.StatisticsXmlTextFilePlotter;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * The initial setup frame of the system's GUI
 *
 * @author Alessio Addimando
 * @author Chiara Ceccarini
 */
public class MapForm extends JFrame {

    private final Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
    private static final int MIN_NUMBER_OF_SEMAPHORES = 4;
    private static final int MAX_NUMBER_OF_SEMAPHORES = 10;
    private static final int MULTIPLIER = 15;
    private final int PADDING = dimension.height / 50;
    private static final String ERROR_MESSAGE = "Error";
    private static final int INPUT_WIDTH = 8;
    private static final String COMPARISON = "Comparison mode";
    private static final String STANDARD = "Standard mode";
    private static final String[] modalityChoices = {COMPARISON, STANDARD};

    private JPanel formPanel;
    private JLabel welcomeLabel;
    private JLabel formatLabel;
    private JButton startButton;
    private JButton plotButton;
    private JTextField numberOfSemaphoresInput;
    private JComboBox<String> comboBox;

    private Props mapViewerProps;
    private Props comparisonViewerProps;
    private Props cityProps;

    private ActorSystem system;

    /**
     * @param mapViewerProps        the Props of the MapViewer
     * @param comparisonViewerProps the Props of the ComparisonViewer
     * @param cityProps             the Props of the City
     * @param system                the System
     */
    public MapForm(Props mapViewerProps, Props comparisonViewerProps, Props cityProps, ActorSystem system) {

        this.mapViewerProps = mapViewerProps;
        this.comparisonViewerProps = comparisonViewerProps;
        this.cityProps = cityProps;
        this.system = system;

        EventQueue.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException |
                    InstantiationException |
                    IllegalAccessException |
                    UnsupportedLookAndFeelException ex) {
                ex.printStackTrace();
            }

            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            this.setTitle("Upuaut");
            getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));

            this.formPanel = new JPanel();
            this.welcomeLabel = new JLabel("Welcome to UPUAUT!");
            welcomeLabel.setFont(new Font("Verdana", Font.BOLD, 50));
            welcomeLabel.setForeground(new Color(75, 198, 83));
            welcomeLabel.setBorder(new EmptyBorder(0, 0, PADDING, 0));
            this.formatLabel = new JLabel(
                    "<html>The number of SEMAPHORES must be between "
                            + MIN_NUMBER_OF_SEMAPHORES + " and " + MAX_NUMBER_OF_SEMAPHORES +
                            "</html>", SwingConstants.CENTER);
            formatLabel.setBorder(new EmptyBorder(0, 0, PADDING, 0));

            JLabel numberOfSemaphoresLabel = new JLabel("How many semaphores?");
            this.numberOfSemaphoresInput = new JTextField();
            this.numberOfSemaphoresInput.setColumns(INPUT_WIDTH);

            comboBox = new JComboBox<>(modalityChoices);
            comboBox.setSelectedItem(STANDARD);
            comboBox.setVisible(true);

            add(comboBox);

            final KeyListener enterListener = initKeyListener();

            numberOfSemaphoresInput.addKeyListener(enterListener);
            comboBox.addKeyListener(enterListener);

            this.startButton = new JButton("START");
            this.plotButton = new JButton("PLOT OLD STATISTICS");

            formPanelBuilder(numberOfSemaphoresLabel);

            plotButton.setAlignmentX(Component.CENTER_ALIGNMENT);

            ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
            URL resource = classLoader.getResource
                    ("images/iconaUpuautMID2.png");

            JLabel iconLabel = new JLabel(new ImageIcon(resource));
            iconLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
            iconLabel.setBorder(new EmptyBorder(0, 0, PADDING, 0));

            welcomeLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
            formatLabel.setAlignmentX(Component.CENTER_ALIGNMENT);

            add(welcomeLabel);
            add(iconLabel);
            add(formatLabel);
            add(formPanel);
            add(plotButton);

            this.setVisible(true);
            this.setResizable(true);
            this.pack();
            RefineryUtilities.centerFrameOnScreen(this);

            final int x = dimension.width * 5 / 9;
            final int y = dimension.height * 7 / 15;
            this.setSize(new Dimension(x, y));
            setMinimumSize(new Dimension(x, y));

            this.startButton.addActionListener(e -> startListener());
            this.plotButton.addActionListener(e -> plotListener());
        });
    }

    /**
     * Listener that starts when the plot button pressed by the user
     */
    private void plotListener() {
        new StatisticsXmlTextFilePlotter();
    }

    /**
     * Listener that starts when the start button or the enter key is pressed by the user
     */
    private void startListener() {
        if (!numberOfSemaphoresInput.getText().isEmpty()) {
            int numberOfSemaphores = Integer.parseInt(numberOfSemaphoresInput.getText());
            if (comboBox.getSelectedItem() != null &&
                    numberOfSemaphores >= MIN_NUMBER_OF_SEMAPHORES && numberOfSemaphores <= MAX_NUMBER_OF_SEMAPHORES) {
                switch (comboBox.getSelectedItem().toString()) {
                    case STANDARD:
                        ActorRef mapViewer = system.actorOf(this.mapViewerProps);
                        ActorRef upuautCity = system.actorOf(this.cityProps);
                        mapViewer.tell(new InitCityInMapViewerMessage(upuautCity), ActorRef.noSender());
                        mapViewer.tell(new InitCityMessage(numberOfSemaphores,
                                        numberOfSemaphores * numberOfSemaphores * MULTIPLIER,
                                        mapViewer),
                                ActorRef.noSender());
                        system.scheduler().scheduleOnce(Duration.create(500, TimeUnit.MILLISECONDS),
                                mapViewer, new StartMessage(), system.dispatcher(), ActorRef.noSender());
                        this.setVisible(false);
                        break;

                    case COMPARISON:
                        ActorRef comparisonViewer = system.actorOf(this.comparisonViewerProps);
                        upuautCity = system.actorOf(this.cityProps);
                        ActorRef standardCity = system.actorOf(this.cityProps);
                        comparisonViewer.tell(new InitCitiesInComparisonViewerMessage(upuautCity, standardCity),
                                ActorRef.noSender());
                        comparisonViewer.tell(new InitCityMessage(numberOfSemaphores,
                                numberOfSemaphores * numberOfSemaphores * MULTIPLIER * 4,
                                comparisonViewer), ActorRef.noSender());
                        system.scheduler().scheduleOnce(Duration.create(500, TimeUnit.MILLISECONDS),
                                comparisonViewer, new StartMessage(), system.dispatcher(), ActorRef.noSender());
                        this.setVisible(false);
                        break;
                }
            } else {
                showErrorDialog();
            }
        } else {
            showErrorDialog();
        }
    }

    /**
     * Shows an error dialog for the wrong input
     */
    private void showErrorDialog() {
        JOptionPane.showMessageDialog(formPanel,
                "Error: The number of SEMAPHORES must be between "
                        + MIN_NUMBER_OF_SEMAPHORES + " and "
                        + MAX_NUMBER_OF_SEMAPHORES, ERROR_MESSAGE,
                JOptionPane.ERROR_MESSAGE);
        numberOfSemaphoresInput.setText("");
    }

    /**
     * Adds a behaviour when the enter key of the keyboard is pressed
     *
     * @param event event of the key pressed
     */
    private void enterKeyPressed(KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.VK_ENTER) {
            startListener();
        }
    }

    /**
     * Initializes the MapForm's keyListener
     *
     * @return the new keyListener
     */
    private KeyListener initKeyListener() {
        return new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                enterKeyPressed(e);
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        };
    }

    /**
     * Adds components to formPanel in a compact way
     *
     * @param numberOfSemaphoresLabel the Label for the number of the Semaphores
     */
    private void formPanelBuilder(JLabel numberOfSemaphoresLabel) {
        this.formPanel.setLayout(new FlowLayout());
        formPanel.setBorder(new EmptyBorder(0, 0, PADDING, 0));
        this.formPanel.add(numberOfSemaphoresLabel);
        this.formPanel.add(numberOfSemaphoresInput);
        this.formPanel.add(comboBox);
        this.formPanel.add(startButton);
        formPanel.setAlignmentY(Component.CENTER_ALIGNMENT);
    }
}
