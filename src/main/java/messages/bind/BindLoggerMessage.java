package messages.bind;

import akka.actor.ActorRef;

/**
 * Message necessary to bind the Logger Actor to other Actors of the System. Sent by the City to the other Actors.
 *
 * @author Alberto Mulazzani
 */
public final class BindLoggerMessage {

    private ActorRef loggerRef;

    /**
     * @param loggerRef ActorRef of the Logger Actor
     */
    public BindLoggerMessage(ActorRef loggerRef) {
        this.loggerRef = loggerRef;
    }

    public ActorRef getLoggerRef() {
        return loggerRef;
    }
}
