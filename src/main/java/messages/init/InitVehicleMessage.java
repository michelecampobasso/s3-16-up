package messages.init;

import akka.actor.ActorRef;

import java.util.List;

/**
 * Message from the VehicleDispenser to the Vehicle to init it
 *
 * @author Enrico Cagnazzo
 */
public final class InitVehicleMessage {
    private final ActorRef startPosition;
    private final List<ActorRef> destination;
    private final ActorRef cityActor;

    /**
     * @param startPosition the initial position of the Vehicle
     * @param destination   the final destination of the Vehicle
     * @param cityActor     the reference to the City Actor
     */
    public InitVehicleMessage(ActorRef startPosition, List<ActorRef> destination, ActorRef cityActor) {
        this.startPosition = startPosition;
        this.destination = destination;
        this.cityActor = cityActor;
    }

    public ActorRef getStartPosition() {
        return startPosition;
    }

    public List<ActorRef> getDestination() {
        return destination;
    }

    public ActorRef getCityActor() {
        return cityActor;
    }
}
