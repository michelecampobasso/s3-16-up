package utilities;

import akka.actor.ActorRef;
import scala.collection.mutable.HashMap;

/**
 * Class that represents a Semaphore Actor status with his Incoming and Outgoing lanes.
 *
 * @author Enrico Cagnazzo
 * @author Chiara Ceccarini
 * @author Michele Campobasso
 */
public class SemaphoreStatus {
    private HashMap<ActorRef, LaneStatus> incomingLanes;
    private HashMap<ActorRef, LaneStatus> outgoingLanes;

    /**
     * @param incoming map of the Incoming Lane Actors ActorRefs in a Semaphore Actor with their Statuses.
     * @param outgoing map of the Outgoing Lane Actors ActorRefs in a Semaphore Actor with their Statuses.
     */
    public SemaphoreStatus(HashMap<ActorRef, LaneStatus> incoming, HashMap<ActorRef, LaneStatus> outgoing) {
        incomingLanes = incoming;
        outgoingLanes = outgoing;
    }

    public HashMap<ActorRef, LaneStatus> getOutgoingLanes() {
        return outgoingLanes;
    }

    public HashMap<ActorRef, LaneStatus> getIncomingLanes() {
        return incomingLanes;
    }
}
