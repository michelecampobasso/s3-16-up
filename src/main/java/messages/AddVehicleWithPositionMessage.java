package messages;

import utilities.LanePosition;

/**
 * Message sent by the city to the vehicle dispenser to create a vehicle with the specified positions of start
 * and destination.
 *
 * @author Enrico Cagnazzo
 */
public final class AddVehicleWithPositionMessage {
    private LanePosition startVehiclePosition;
    private LanePosition destinationVehiclePosition;

    public AddVehicleWithPositionMessage(LanePosition startVehiclePosition, LanePosition destinationVehiclePosition) {
        this.startVehiclePosition = startVehiclePosition;
        this.destinationVehiclePosition = destinationVehiclePosition;
    }

    public LanePosition getStartVehiclePosition() {
        return startVehiclePosition;
    }

    public LanePosition getDestinationVehiclePosition() {
        return destinationVehiclePosition;
    }
}
