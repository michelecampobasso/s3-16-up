package messages.init;

import akka.actor.ActorRef;
import utilities.enumerations.Direction;

import java.util.ArrayList;
import java.util.Map;

/**
 * Message sent to init the VehicleDispenser.
 *
 * @author Enrico Cagnazzo
 */
public final class InitVehicleDispenserMessage {
    private ArrayList<ArrayList<Map<Direction, ActorRef>>> lanes;

    /**
     * @param lanes the lanes of the City
     */
    public InitVehicleDispenserMessage(ArrayList<ArrayList<Map<Direction, ActorRef>>> lanes) {
        this.lanes = lanes;
    }

    public ArrayList<ArrayList<Map<Direction, ActorRef>>> getLanes() {
        return lanes;
    }

}
