package messages;

/**
 * Message sent from a Vehicle Actor to a Lane Actor when the Vehicle Actor leaves the Lane Actor.
 *
 * @author Alberto Mulazzani
 * @author Chiara Ceccarini
 */
public final class LeftLaneMessage {
}
