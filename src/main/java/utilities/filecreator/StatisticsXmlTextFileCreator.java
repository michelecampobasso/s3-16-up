package utilities.filecreator;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
import utilities.LaneIdentifier;
import utilities.exceptions.StatisticsXmlFileCreationException;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import static utilities.filecreator.XmlFileCreatorConstants.*;

/**
 * Implementation of the TextFileCreator that creates an XML mapping of system's general statistics
 *
 * @author Alessio Addimando
 */
public class StatisticsXmlTextFileCreator implements TextFileCreator<LaneIdentifier, Double, String> {

    /**
     * @param laneStatistics A map with the statics of each lane
     * @return a true bool if the conversion finished without exceptions, else false
     * @throws StatisticsXmlFileCreationException thrown when there's an error in the creation of the file
     */
    public String writeToFile(Map<LaneIdentifier, Double> laneStatistics) throws StatisticsXmlFileCreationException {
        Optional<String> filename = Optional.empty();
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Iterator<Map.Entry<LaneIdentifier, Double>> mapIterator = laneStatistics.entrySet().iterator();
            Document doc = docBuilder.newDocument();

            Element rootElement = doc.createElement(STATISTICS);
            doc.appendChild(rootElement);


            while (mapIterator.hasNext()) {
                Map.Entry<LaneIdentifier, Double> currentPair = mapIterator.next();

                Element lane = doc.createElement(LANE);
                rootElement.appendChild(lane);

                Element originSemaphore = doc.createElement(ORIGIN_SEMAPHORE);
                originSemaphore.appendChild(doc.createTextNode((currentPair.getKey().getSemaphoreFrom().path().name())));
                lane.appendChild(originSemaphore);

                Element destinationSemaphore = doc.createElement(DESTINATION_SEMAPHORE);
                destinationSemaphore.appendChild(doc.createTextNode((currentPair.getKey().getSemaphoreTo().path().name())));
                lane.appendChild(destinationSemaphore);

                Element averageValue = doc.createElement(AVERAGE_TRAFFIC);
                averageValue.appendChild(doc.createTextNode(currentPair.getValue().toString()));
                lane.appendChild(averageValue);

            }
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            filename = getFileName();
            if (filename.isPresent()) {
                StreamResult result = new StreamResult(new File(filename.get()));
                transformer.transform(source, result);
            } else {
                throw new StatisticsXmlFileCreationException();
            }
        } catch (TransformerException | ParserConfigurationException e) {
            e.printStackTrace();
        }
        return filename.orElse("");
    }

    /**
     * Method that returns the fileName of the chosen statistic file.
     * It opens the dialog to chose the wanted position for the file
     *
     * @return the fileName of the chosen statistic file
     */
    private Optional<String> getFileName() {
        JFileChooser fileChooser = new JFileChooser(System.getProperty(USER_HOME));
        FileNameExtensionFilter xmlFilter = new FileNameExtensionFilter(XML_LABEL, XML, XML);
        fileChooser.setAcceptAllFileFilterUsed(true);
        fileChooser.addChoosableFileFilter(xmlFilter);
        fileChooser.setSelectedFile(new File(
                UPUAUT_STATISTIC +
                        LocalDateTime.now().toLocalDate() +
                        UNDER_SCORE +
                        LocalDateTime.now().getHour() +
                        LocalDateTime.now().getMinute()));
        int returnVal = fileChooser.showSaveDialog(new JFrame());
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            Optional<String> tempFile = Optional.of(fileChooser.getSelectedFile().toString());
            if (!tempFile.get().endsWith(XML)) {
                String convertedStringToXml = tempFile.get();
                convertedStringToXml += XML;
                return Optional.of(convertedStringToXml);
            }
            return tempFile;
        }
        return Optional.empty();
    }
}