package entities.dummyTestUtility

import akka.actor.ActorRef
import entities.AbstractActorWithLogger
import messages.AddVehicleMessage
import messages.gui.ChangeTrafficLoadMessage
import messages.init.InitLoadControllerMessage
import utilities.enumerations.TrafficLoadState


/**
  * LoadController actor that forwards AddVehicleMessages to the cityActor to add new vehicles
  * periodically in the City. It cycles between the 3 phases of Load State.
  *
  * @author Alberto Mulazzani
  */
class LoadController extends AbstractActorWithLogger {

    private var loadChangeCounter: Int = 0
    private val changeLoadValue = 10

    private var loadState: TrafficLoadState = TrafficLoadState.LOW

    private var isFirstTime: Boolean = true
    private var cityActor: ActorRef = _

    /**
      * The LoadController Actor can receive and handle these messages:<br>
      *
      * InitLoadControllerMessage<br>
      * AddVehicleMessage<br>
      */
    override def receive = {

        case message: InitLoadControllerMessage => this.cityActor = message.getCityActor

        case message: AddVehicleMessage =>
            if ((loadChangeCounter % changeLoadValue == 0) && !isFirstTime) {
                loadState = loadState.getNext(loadState)
                sender() ! new ChangeTrafficLoadMessage(loadState)
            }
            isFirstTime = false
            loadChangeCounter += 1
            cityActor ! new AddVehicleMessage(message.getVehiclesNumber * loadState.getLoadMultiplier)
    }
}
