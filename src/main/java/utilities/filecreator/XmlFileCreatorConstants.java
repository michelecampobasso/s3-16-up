package utilities.filecreator;

/**
 * Constants for the XML file's creation.
 */
public class XmlFileCreatorConstants {
    public static final String STATISTICS = "statistics";
    public static final String LANE = "lane";
    public static final String ORIGIN_SEMAPHORE = "Origin-semaphore";
    public static final String DESTINATION_SEMAPHORE = "Destination-semaphore";
    public static final String AVERAGE_TRAFFIC = "average-traffic";
    public static final String XML = ".xml";
    public static final String XML_LABEL = "XML File";
    public static final String UPUAUT_STATISTIC = "UP-STAT_";
    public static final String UNDER_SCORE = "_";
    public static final String USER_HOME = "user.home";
}
