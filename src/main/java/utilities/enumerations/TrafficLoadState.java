package utilities.enumerations;

/**
 * Enum that represents the possible values of a load's state. Each value has a load multiplier.
 *
 * @author Alberto Mulazzani
 */
public enum TrafficLoadState {
    HIGH(3),
    MID(2),
    LOW(1);


    private final int loadMultiplier;

    /**
     * @param loadMultiplier the load multiplier
     */
    TrafficLoadState(int loadMultiplier) {
        this.loadMultiplier = loadMultiplier;
    }

    @Override
    public String toString() {
        return this.name();
    }

    public int getLoadMultiplier() {
        return loadMultiplier;
    }

    /**
     * @param state the current TrafficLoadState
     * @return the next TrafficLoadState in the cycle
     */
    public TrafficLoadState getNext(TrafficLoadState state) {
        switch (state) {
            case HIGH:
                return LOW;
            case MID:
                return HIGH;
            case LOW:
                return MID;
        }
        return state;
    }
}
