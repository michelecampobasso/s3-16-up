package gui;

import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.RefineryUtilities;
import utilities.filecreator.StatisticsXmlTextFilePlotter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Dialog that allows to choose to plot statistics dataset or to go back to map
 *
 * @author Alessio Addimando
 */
public class StatisticianResultDialog extends JFrame {

    private final Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();

    /**
     * @param newFileName the file saved
     * @param dataset     to create a chart
     */
    public StatisticianResultDialog(String newFileName, XYSeriesCollection dataset) {
        EventQueue.invokeLater(() -> {
            this.setTitle("File saved successfully!");

            JLabel fileNameLabel = new JLabel("The statistics are saved in: " + newFileName);
            fileNameLabel.setVerticalAlignment(JLabel.CENTER);
            JPanel info = new JPanel();
            info.add(fileNameLabel);

            JButton plotLastDataSet = new JButton("Plot it!");
            JButton closeButton = new JButton("Back to Map");
            JButton plotOtherDataSet = new JButton("Plot old DataSet");
            JPanel buttons = new JPanel();
            buttons.add(plotLastDataSet);
            buttons.add(plotOtherDataSet);
            buttons.add(closeButton);

            plotOtherDataSet.addActionListener(e -> new StatisticsXmlTextFilePlotter());

            closeButton.addActionListener((ActionEvent e) -> super.dispose());
            plotLastDataSet.addActionListener(e -> {
                final StatisticsView statisticsView = new StatisticsView(dataset);
                statisticsView.pack();
                RefineryUtilities.centerFrameOnScreen(statisticsView);
                this.dispose();
                statisticsView.setVisible(true);
            });

            this.add(BorderLayout.CENTER, info);
            this.add(BorderLayout.SOUTH, buttons);
            setMinimumSize(new Dimension(dimension.width * 2 / 5, dimension.height / 5));
            this.setVisible(true);
            this.pack();
            RefineryUtilities.centerFrameOnScreen(this);
        });
    }
}
