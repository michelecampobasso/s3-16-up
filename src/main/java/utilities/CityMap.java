package utilities;

import akka.actor.ActorRef;

import java.util.HashMap;
import java.util.Map;

/**
 * Class that rapresents the Map of the City
 *
 * @author Enrico Cagnazzo
 */
public class CityMap {
    private Map<ActorRef, NextPossibleLanes> map = new HashMap<>();

    public CityMap() {
    }

    public Map<ActorRef, NextPossibleLanes> getMap() {
        return map;
    }

    public void setMap(Map<ActorRef, NextPossibleLanes> map) {
        this.map = map;
    }

    /**
     * Add in the city map a lane and the adjacent lanes that a vehicle can take
     *
     * @param lane              the lane to add
     * @param nextPossibleLanes the adjacent lanes that a vehicle can take from lane
     */
    public void addLane(ActorRef lane, NextPossibleLanes nextPossibleLanes) {
        this.map.put(lane, nextPossibleLanes);
    }
}
