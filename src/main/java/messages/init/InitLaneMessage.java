package messages.init;

import akka.actor.ActorRef;

/**
 * Message necessary to initialize a Lane Actor. Sent from the City Actor to a Lane Actor.
 *
 * @author Alberto Mulazzani
 * @author Chiara Ceccarini
 */
public final class InitLaneMessage {

    private ActorRef semaphoreFrom;
    private ActorRef semaphoreTo;
    private int capacity;

    /**
     * @param semaphoreFrom ActorRef of the Semaphore Actor where the Lane Actor starts.
     * @param semaphoreTo   ActorRef of the Semaphore Actor where the Lane Actor ends.
     * @param capacity      capacity of the Lane Actor.
     */

    public InitLaneMessage(ActorRef semaphoreFrom, ActorRef semaphoreTo, int capacity) {
        this.semaphoreFrom = semaphoreFrom;
        this.semaphoreTo = semaphoreTo;
        this.capacity = capacity;
    }


    public ActorRef getSemaphoreFrom() {
        return semaphoreFrom;
    }

    public ActorRef getSemaphoreTo() {
        return semaphoreTo;
    }

    public int getCapacity() {
        return capacity;
    }

}
