package entities

import java.util
import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, Cancellable}
import messages._
import messages.bind.BindLoggerMessage
import messages.init.InitVehicleMessage
import utilities.CityMap
import utilities.enumerations.{LaneReserveResponse, LoggerPriority, SemaphoreColor}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.FiniteDuration

/**
  * Vehicle Actor. It goes from a StartPosition to a Destination and it occupies Lane Actors
  *
  * @author Chiara Ceccarini
  * @author Enrico Cagnazzo
  */
class Vehicle extends AbstractActorWithLogger {

    private var cityActor: ActorRef = _
    private var cityMap: CityMap = _

    private var currentPosition: Option[ActorRef] = None
    private var startPosition: ActorRef = _
    private var nextDirection: ActorRef = _
    private var destination: util.List[ActorRef] = _
    private var pathToDestination: mutable.ArrayBuffer[ActorRef] = _

    private var lanternColor: SemaphoreColor = SemaphoreColor.GREEN
    private var requestSent: Boolean = false
    private var hasReachedRoadCrossing: Boolean = false
    private var travellingTime: Cancellable = _

    private var beginTravelTime: Long = _

    startTravellingTime()

    /**
      * A Vehicle Actor can receive and handle these messages:<br>
      *
      * BindLoggerMessage<br>
      * InitVehicleMessage<br>
      * MapResponseMessage<br>
      * SemaphoreColorMessage<br>
      * LaneReserveResponseMessage<br>
      * NotMoreFullMessage<br>
      * ReachedRoadCrossingMessage<br>
      */
    override def receive = {

        case message: BindLoggerMessage => loggerRef = message.getLoggerRef

        case message: InitVehicleMessage =>
            destination = message.getDestination
            startPosition = message.getStartPosition
            cityActor = message.getCityActor
            cityActor ! new MapRequestMessage

        case message: MapResponseMessage =>
            printLog("MapResponseMessage received", LoggerPriority.LOW)
            cityMap = message.getCityMap
            pathToDestination = calculatePathToDestination(startPosition, destination)
            nextDirection = pathToDestination.remove(0)
            beginTravelTime = System.currentTimeMillis
            sendMessageToNextDirection()

        case message: SemaphoreColorMessage =>
            printLog("SemaphoreColorMessage received", LoggerPriority.LOW)
            if (currentPosition.nonEmpty && sender() == currentPosition.get) {
                lanternColor = message.getStatus
                if (lanternColor == SemaphoreColor.GREEN && hasReachedRoadCrossing)
                    sendMessageToNextDirection()
            }

        case message: LaneReserveResponseMessage =>
            printLog("LaneReserveResponseMessage received", LoggerPriority.LOW)
            requestSent = false
            if (sender() == nextDirection) {
                if (message.getStatus == LaneReserveResponse.FREE) {
                    if (lanternColor == SemaphoreColor.GREEN) {
                        sender() ! new LaneReserveMessage
                        if (currentPosition.nonEmpty)
                            currentPosition.get ! new LeftLaneMessage
                        currentPosition = new Some[ActorRef](nextDirection)
                        if (pathToDestination.nonEmpty)
                            nextDirection = pathToDestination.remove(0)
                        else
                            parkTheVehicle()
                        startTravellingTime()
                    } else
                        sender() ! new LaneNotReserveMessage
                }
            }

        case _: NotMoreFullMessage =>
            if (sender() == nextDirection && lanternColor == SemaphoreColor.GREEN && hasReachedRoadCrossing)
                sender() ! new LaneReserveRequestMessage

        case _: ReachedRoadCrossingMessage =>
            hasReachedRoadCrossing = true
            if (lanternColor == SemaphoreColor.GREEN)
                sendMessageToNextDirection()
    }

    /**
      * Calculates the path to the destination using the BFS
      *
      * @param start       the lane from where the vehicle starts
      * @param destination an array with two opposite lanes (x to y and y to x)
      * @return an array with the sequence of the lanes that the vehicle have to take to reach the destination
      */
    private def calculatePathToDestination(start: ActorRef, destination: util.List[ActorRef]): ArrayBuffer[ActorRef] = {
        var possiblePaths: mutable.Queue[ArrayBuffer[ActorRef]] = new mutable.Queue[ArrayBuffer[ActorRef]]()
        var actualPath: ArrayBuffer[ActorRef] = new ArrayBuffer[ActorRef]()
        var visited: mutable.Set[ActorRef] = new mutable.HashSet[ActorRef] {}
        actualPath += start
        possiblePaths += actualPath
        visited += start
        while (true) {
            actualPath = possiblePaths.dequeue()
            val nextLanes = cityMap.getMap.get(actualPath.last).getNextLanes
            for (i <- 0 until nextLanes.size()) {
                if (!visited.contains(nextLanes.get(i))) {
                    visited += nextLanes.get(i)
                    val newPath = actualPath.clone()
                    newPath += nextLanes.get(i)
                    if (destination.contains(nextLanes.get(i)))
                        return newPath
                    else
                        possiblePaths.enqueue(newPath)
                }
            }
        }
        new ArrayBuffer[ActorRef]()
    }

    /**
      * Method to send a LaneReserveRequestMessage to the next Lane Actor in the pathToDestination ArrayBuffer
      */
    private def sendMessageToNextDirection(): Unit = {
        if (!requestSent) {
            context.system.scheduler.scheduleOnce(FiniteDuration.apply(500, TimeUnit.MILLISECONDS)) {
                if (pathToDestination.nonEmpty)
                    nextDirection ! new LaneReserveRequestMessage(pathToDestination.head)
                else
                    nextDirection ! new LaneReserveRequestMessage()
            }
            requestSent = true
        }
    }

    /**
      * Method to communicate to the City Actor that the Vehicle Actor reached his destination and to stop himself
      */
    private def parkTheVehicle(): Unit = {
        currentPosition.get ! new LeftLaneMessage
        printLog(" reached his destination", LoggerPriority.LOW)
        cityActor ! new DestinationReachedMessage(((System.currentTimeMillis - beginTravelTime) / 1000).toInt)
        context.stop(self)
    }

    /**
      * Method that starts the timer representing the in-lane travelling time by a Vehicle
      */
    private def startTravellingTime(): Unit = {
        travellingTime = context.system.scheduler.scheduleOnce(FiniteDuration.apply(3000, TimeUnit.MILLISECONDS)) {
            self ! new ReachedRoadCrossingMessage()
        }
        hasReachedRoadCrossing = false
    }
}