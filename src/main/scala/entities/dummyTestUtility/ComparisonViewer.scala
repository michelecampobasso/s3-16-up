package entities.dummyTestUtility

import java.util

import akka.actor.{AbstractActor, ActorRef}
import gui._
import messages._
import messages.bind.BindStandardCityMessage
import messages.init.{InitCitiesInComparisonViewerMessage, InitCityMessage, InitViewMessage}
import messages.utility.{AverageTravelTimeMessage, IgnoreTrafficConditionsMessage, StartMessage}
import utilities.enumerations.Direction

/**
  * ComparisonViewer Actor. It receives messages from all the other Actors in the System and update the GUI
  *
  * @author Michele Campobasso
  */
class ComparisonViewer extends AbstractActor {

    private var comparisonView: ComparisonView = _

    private var lanes: util.ArrayList[util.ArrayList[util.Map[Direction, ActorRef]]] = _
    private var semaphores: util.ArrayList[util.ArrayList[ActorRef]] = _

    private var statistician: ActorRef = _
    private var standardCityActor: ActorRef = _
    private var upuautCityActor: ActorRef = _

    private var viewAlreadyUp: Boolean = false

    /**
      * The MapViewer actor can receive and handle these messages:<br>
      *
      * InitViewMessage<br>
      * InitCitiesInComparisonViewerMessage<br>
      * InitCityMessage<br>
      * StartMessage<br>
      * DestinationReachedMessage<br>
      * AverageTravelTimeMessage<br>
      */
    override def receive = {

        case message: InitViewMessage =>
            lanes = message.getLanes
            semaphores = message.getSemaphores
            statistician = message.getStatistician
            if (!viewAlreadyUp) {
                comparisonView = new ComparisonView(message.getNumberOfVehicles)
                viewAlreadyUp = true
            }

        case message: InitCitiesInComparisonViewerMessage =>
            standardCityActor = message.getStandardCityActor
            upuautCityActor = message.getUpuautCityActor
            upuautCityActor ! new BindStandardCityMessage(standardCityActor)

        case message: InitCityMessage =>
            standardCityActor ! new IgnoreTrafficConditionsMessage
            standardCityActor forward message
            upuautCityActor forward message

        case message: StartMessage =>
            upuautCityActor forward message
            standardCityActor forward message
            upuautCityActor ! new CreateFirstsVehiclesMessage

        case message: DestinationReachedMessage =>
            if (message.isStandardCity) comparisonView.updateNumberOfArrivedVehiclesInStandardCity()
            else comparisonView.updateNumberOfArrivedVehiclesInUpuautCity()

        case message: AverageTravelTimeMessage =>
            if (message.isStandardCity) comparisonView.setAverageTravelTimeInStandardCity(message.getAverageTime)
            else comparisonView.setAverageTravelTimeInUpuautCity(message.getAverageTime)
    }
}

