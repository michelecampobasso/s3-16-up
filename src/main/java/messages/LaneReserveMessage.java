package messages;

/**
 * Message sent from a Vehicle Actor to a Lane Actor to enter the Lane Actor.
 *
 * @author Alberto Mulazzani
 * @author Chiara Ceccarini
 */
public final class LaneReserveMessage {

}
