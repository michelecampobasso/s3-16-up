package messages;

/**
 * Message sent from a Lane Actor to waiting Vehicle Actors when the Lane Actor is Free.
 *
 * @author Alberto Mulazzani
 * @author Chiara Ceccarini
 */
public final class NotMoreFullMessage {
}
