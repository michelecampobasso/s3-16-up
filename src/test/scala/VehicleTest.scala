import java.util
import java.util.concurrent.TimeUnit

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import entities.Vehicle
import entities.utility.Logger
import messages._
import messages.bind.BindLoggerMessage
import messages.init.InitVehicleMessage
import mock.CityAndVehicleDispenserMock
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import utilities.CityMap
import utilities.enumerations._

import scala.concurrent.duration.FiniteDuration

/**
  * @author Alberto Mulazzani
  * @author Michele Campobasso
  */
class VehicleTest extends TestKit(ActorSystem("MySpec"))
    with ImplicitSender
    with WordSpecLike
    with BeforeAndAfterAll
    with Matchers {

    private val logger = system.actorOf(Props[Logger])
    private var vehicleActor: ActorRef = _
    private var cityMap: CityMap = _
    private var semaphores: util.ArrayList[util.ArrayList[ActorRef]] = new util.ArrayList[util.ArrayList[ActorRef]]()
    private var lanes: util.ArrayList[util.ArrayList[util.Map[Direction, ActorRef]]] =
        new util.ArrayList[util.ArrayList[util.Map[Direction, ActorRef]]]()
    private val mock: CityAndVehicleDispenserMock = new CityAndVehicleDispenserMock

    override def afterAll {
        TestKit.shutdownActorSystem(system)
    }

    override def beforeAll: Unit = {
        mock.actorSystemInit(system)
        semaphores = mock.semaphoreInit
        lanes = mock.lanesInit
        cityMap = mock.calculateMap
    }

    def vehicleInitMessageSequence(): Unit = {
        vehicleActor = system.actorOf(Props[Vehicle])
        vehicleActor ! new BindLoggerMessage(logger)
        vehicleActor ! new InitVehicleMessage(mock.chooseStartPosition, mock.chooseDestination, self)
        receiveOne(new FiniteDuration(5000, TimeUnit.MILLISECONDS))
        vehicleActor ! new MapResponseMessage(cityMap)
    }

    "A Vehicle actor" must {

        "not reply to SemaphoreColorMessage if green" in {
            vehicleInitMessageSequence()
            vehicleActor ! new SemaphoreColorMessage(SemaphoreColor.GREEN)
            expectNoMsg()
        }

        "not reply to SemaphoreColorMessage if red" in {
            vehicleInitMessageSequence()
            vehicleActor ! new SemaphoreColorMessage(SemaphoreColor.RED)
            expectNoMsg()
        }

        "not reply to LaneReserveResponseMessage if blocked" in {
            vehicleInitMessageSequence()
            vehicleActor ! new LaneReserveResponseMessage(LaneReserveResponse.BLOCKED)
            expectNoMsg()
        }

        "not reply to NotMoreFullMessage but the sender lane is not correct" in {
            vehicleInitMessageSequence()
            vehicleActor ! new SemaphoreColorMessage(SemaphoreColor.GREEN)
            vehicleActor ! new NotMoreFullMessage()
            expectNoMsg()
        }

        "not reply to LaneReserveResponseMessage if free but the sender lane is not correct" in {
            vehicleInitMessageSequence()
            vehicleActor ! new SemaphoreColorMessage(SemaphoreColor.GREEN)
            vehicleActor ! new LaneReserveResponseMessage(LaneReserveResponse.FREE)
            expectNoMsg()
        }
    }
}