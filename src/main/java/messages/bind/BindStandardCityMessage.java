package messages.bind;

import akka.actor.ActorRef;

/**
 * @author Enrico Cagnazzo
 */
public final class BindStandardCityMessage {
    private ActorRef standardCity;

    public BindStandardCityMessage(ActorRef standardCity) {
        this.standardCity = standardCity;
    }

    public ActorRef getStandardCity() {
        return standardCity;
    }
}
