package messages.init;

import akka.actor.ActorRef;

/**
 * Message sent to init the LoadController Actor.
 *
 * @author Alberto Mulazzani
 */
public final class InitLoadControllerMessage {

    private ActorRef cityActor;

    /**
     * @param cityActor the reference to the City Actor
     */
    public InitLoadControllerMessage(ActorRef cityActor) {
        this.cityActor = cityActor;
    }

    public ActorRef getCityActor() {
        return cityActor;
    }
}
