package gui;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;

import javax.swing.*;
import java.awt.*;

/**
 * It's the frame that contains the linear chart plotted
 *
 * @author Alessio Addimando
 */
public class StatisticsView extends JFrame {

    private XYDataset dataset;

    /**
     * @param dataset  XYSeriesCollection to plot on the chart
     * @param filename to show on the main frame of statistics
     */
    public StatisticsView(final XYSeriesCollection dataset, final String filename) {
        super("Upuaut Statistical tool - " + filename);
        this.dataset = dataset;
        this.setStatisticsView();
    }

    /**
     * @param dataset XYSeriesCollection of the last sample extracted to plot on the chart
     */
    public StatisticsView(final XYSeriesCollection dataset) {
        super("Upuaut Statistical tool - last updated dataset");
        this.dataset = dataset;
        this.setStatisticsView();
    }

    /**
     * Sets the main frame with a general XYLineChart
     */
    public void setStatisticsView() {
        final JFreeChart chart = ChartFactory.createXYLineChart(
                "Lanes' average traffic %",
                "Average traffic %",
                "Number of lanes",
                dataset,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        final Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(750, 540));
        setContentPane(chartPanel);
        setMinimumSize(new Dimension(dimension.width * 3 / 7, dimension.height * 2 / 5));
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
}