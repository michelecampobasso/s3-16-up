package messages.bind;

import akka.actor.ActorRef;

/**
 * Message to bind Statistician. Sent by the City Actor to the lanes
 *
 * @author Alessio Addimando
 */
public final class BindStatisticianMessage {

    private ActorRef statistician;

    /**
     * @param statistician the Statistician ActorRef
     */
    public BindStatisticianMessage(ActorRef statistician) {
        this.statistician = statistician;
    }

    public ActorRef getStatistician() {
        return statistician;
    }
}
