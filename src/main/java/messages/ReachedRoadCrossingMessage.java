package messages;

/**
 * Message sent from the Vehicle to itself to notify its arrival to the road crossing
 *
 * @author Michele Campobasso
 */
public final class ReachedRoadCrossingMessage {
}
