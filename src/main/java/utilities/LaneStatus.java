package utilities;

/**
 * Class that represents the Status of a Lane Actor.
 *
 * @author Alberto Mulazzani
 * @author Alessio Addimando
 * @author Chiara Ceccarini
 */
public class LaneStatus {
    private double congestion;

    /**
     * @param congestion congestion of the Lane Actor
     */
    public LaneStatus(double congestion) {
        this.congestion = congestion;
    }

    public double getCongestion() {
        return congestion;
    }

}
