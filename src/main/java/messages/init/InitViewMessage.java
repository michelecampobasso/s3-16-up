package messages.init;

import akka.actor.ActorRef;
import utilities.enumerations.Direction;

import java.util.ArrayList;
import java.util.Map;

/**
 * Message sent from the City to the MapViewer to create the GUI
 *
 * @author Enrico Cagnazzo
 */
public final class InitViewMessage {
    private ArrayList<ArrayList<Map<Direction, ActorRef>>> lanes;
    private ArrayList<ArrayList<ActorRef>> semaphores;

    private ActorRef statistician;
    private Integer numberOfVehicles;

    /**
     * @param lanes            the grid of all the lanes (2D array * Map(Direction->Lane) )
     * @param semaphores       the grid of all the semaphores (2D array)
     * @param statistician     the reference to the Statistician Actor
     * @param numberOfVehicles the number of Vehicles in the City
     */
    public InitViewMessage(ArrayList<ArrayList<Map<Direction, ActorRef>>> lanes, ArrayList<ArrayList<ActorRef>> semaphores,
                           ActorRef statistician, Integer numberOfVehicles) {
        this.lanes = lanes;
        this.semaphores = semaphores;
        this.statistician = statistician;
        this.numberOfVehicles = numberOfVehicles;
    }

    public ArrayList<ArrayList<Map<Direction, ActorRef>>> getLanes() {
        return lanes;
    }

    public ArrayList<ArrayList<ActorRef>> getSemaphores() {
        return semaphores;
    }

    public ActorRef getStatistician() {
        return statistician;
    }

    public Integer getNumberOfVehicles() {
        return numberOfVehicles;
    }
}
