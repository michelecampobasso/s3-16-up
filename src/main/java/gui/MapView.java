package gui;

import akka.actor.ActorRef;
import messages.AddVehicleMessage;
import messages.utility.RequestStatisticsFileMessage;
import utilities.LaneStatus;
import utilities.enumerations.Direction;
import utilities.enumerations.SemaphoreDirection;
import utilities.enumerations.SemaphoreStrategy;
import utilities.enumerations.TrafficLoadState;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * The main frame implementation of the system's GUI
 *
 * @author Alessio Addimando
 * @author Enrico Cagnazzo
 * @author Chiara Ceccarini
 * @author Alberto Mulazzani
 */
public class MapView extends JFrame {

    private static final int ELEMENT_HEIGHT = 40;
    private final Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
    private final int buttonWidth = getContentPane().getWidth() / 2;

    private int numberOfArrivedVehicles = 0;

    private final ArrayList<ArrayList<Map<Direction, ActorRef>>> lanes;
    private final ArrayList<ArrayList<ActorRef>> semaphores;

    private JLabel numberOfVehicles = new JLabel();
    private JLabel numberOfArrivedVehiclesLabel = new JLabel();
    private JLabel trafficLoad = new JLabel("", SwingConstants.CENTER);
    private CityMapPainter cityMapPainter = new CityMapPainter();

    private ActorRef mapViewer;

    /**
     * @param cityMapPainter the Painter of the City
     * @param lanes          the Lanes of the City
     * @param semaphores     the Semaphores of the City
     * @param mapViewer      the MapViewer Actor
     */
    public MapView(CityMapPainter cityMapPainter, ArrayList<ArrayList<Map<Direction, ActorRef>>> lanes,
                   ArrayList<ArrayList<ActorRef>> semaphores, ActorRef mapViewer) {

        this.cityMapPainter = cityMapPainter;
        this.lanes = lanes;
        this.semaphores = semaphores;
        this.mapViewer = mapViewer;

        final Runnable repaintTask = () -> EventQueue.invokeLater(this::repaint);

        final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(repaintTask, 500, 750, TimeUnit.MILLISECONDS);

        EventQueue.invokeLater(() -> {
            try {
                UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            } catch (ClassNotFoundException |
                    InstantiationException |
                    IllegalAccessException |
                    UnsupportedLookAndFeelException ex) {
                ex.printStackTrace();
            }

            setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
            this.setTitle("Upuaut");
            setLayout(new BorderLayout());

            JPanel numberOfVehiclesPanel = createNumberOfVehiclePanel();
            JPanel buttons = createButtonsPanel();

            JPanel panel = new JPanel();
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            panel.add(numberOfVehiclesPanel);
            panel.add(buttons);

            this.add(panel);

            pack();
            setResizable(true);
            setMinimumSize(new Dimension(dimension.width * 3 / 7, dimension.height * 2 / 5));
            setVisible(true);
            setExtendedState(JFrame.MAXIMIZED_BOTH);

        });
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);
        this.cityMapPainter.paintMap(g, lanes, semaphores, getWidth(), getHeight());
    }

    /**
     * Updates the graphics in cityMapPainter and then repaints the GUI
     *
     * @param lane   the lane to update
     * @param status the new status of the lane
     */
    public void updateLane(ActorRef lane, LaneStatus status) {
        this.cityMapPainter.updateLane(lane, status);
    }

    /**
     * Updates the graphics in cityMapPainter and then repaints the GUI
     *
     * @param semaphore      the semaphore to update
     * @param greenDirection the direction where the semaphore is green
     */
    public void updateSemaphoreLights(ActorRef semaphore, SemaphoreDirection greenDirection) {
        this.cityMapPainter.updateSemaphoreLights(semaphore, greenDirection);
    }

    /**
     * Updates the graphics in cityMapPainter and then repaints the GUI.
     *
     * @param semaphore the semaphore to update
     * @param strategy  the semaphore's Strategy
     */
    public void updateSemaphoreStrategy(ActorRef semaphore, SemaphoreStrategy strategy) {
        this.cityMapPainter.updateSemaphoreStrategy(semaphore, strategy);
    }

    /**
     * Updates the number of vehicles in the GUI.
     *
     * @param numberOfVehicles number of vehicles in the City
     */
    public void updateNumberOfVehicles(int numberOfVehicles) {
        EventQueue.invokeLater(() -> this.numberOfVehicles.setText(Integer.toString(numberOfVehicles)));
    }

    /**
     * Updates the Traffic Load Label to a new state.
     *
     * @param state the state to update the current state with
     */
    public void updateTrafficLoad(TrafficLoadState state) {
        switch (state) {
            case HIGH:
                trafficLoad.setBackground(new Color(237, 99, 99));
                trafficLoad.setText("Heavy load");
                break;
            case MID:
                trafficLoad.setBackground(new Color(229, 155, 64));
                trafficLoad.setText("Medium load");
                break;
            case LOW:
                trafficLoad.setBackground(new Color(75, 198, 83));
                trafficLoad.setText("Soft load");
                break;
        }
    }

    /**
     * Updates the number of arrived Vehicles shown on GUI
     */
    public void updateNumberOfArrivedVehicles() {
        EventQueue.invokeLater(() -> this.numberOfArrivedVehiclesLabel.setText(
                Integer.toString(++numberOfArrivedVehicles)));
    }

    /**
     * Creates the Panel with the information of the number of Vehicles in the City
     *
     * @return the Panel
     */
    private JPanel createNumberOfVehiclePanel() {
        JPanel numberOfVehiclesPanel = new JPanel();
        JLabel numberOfVehiclesText = new JLabel("Number of active vehicles: ");
        JLabel numberOfArrivedVehiclesText = new JLabel("Number of arrived vehicles: ");
        numberOfArrivedVehiclesLabel.setText(Integer.toString(numberOfArrivedVehicles));
        numberOfVehiclesPanel.add(numberOfVehiclesText);
        numberOfVehiclesPanel.add(this.numberOfVehicles);
        numberOfVehiclesPanel.setPreferredSize(new Dimension(dimension.width / 2, ELEMENT_HEIGHT));

        trafficLoad.setOpaque(true);
        numberOfVehiclesPanel.add(trafficLoad);
        numberOfVehiclesPanel.add(numberOfArrivedVehiclesText);
        numberOfVehiclesPanel.add(numberOfArrivedVehiclesLabel);

        return numberOfVehiclesPanel;
    }

    /**
     * Creates the Panel with the addVehicle button and the requestStatistic button
     *
     * @return the Panel
     */
    private JPanel createButtonsPanel() {
        JPanel buttons = new JPanel();
        buttons.setLayout(new BoxLayout(buttons, BoxLayout.X_AXIS));

        JButton addVehicleButton = new JButton("Add vehicle");
        addVehicleButton.setPreferredSize(new Dimension(buttonWidth, ELEMENT_HEIGHT));
        addVehicleButton.addActionListener(e ->
                this.mapViewer.tell(new AddVehicleMessage(), ActorRef.noSender())
        );

        JButton getStatisticButton = new JButton("Request Statistics");
        getStatisticButton.setPreferredSize(new Dimension(buttonWidth, ELEMENT_HEIGHT));
        getStatisticButton.addActionListener(e ->
                this.mapViewer.tell(new RequestStatisticsFileMessage(), ActorRef.noSender())
        );

        buttons.add(addVehicleButton);
        buttons.add(getStatisticButton);

        return buttons;
    }
}