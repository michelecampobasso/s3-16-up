package messages;

/**
 * Message sent from a Vehicle to the Lane when the vehicle can not reserve the lane even if the lane answered with FREE
 * because the semaphore is back red
 *
 * @author Enrico Cagnazzo
 */
public final class LaneNotReserveMessage {
}
