package messages.bind;

import akka.actor.ActorRef;
import utilities.enumerations.Direction;
import utilities.enumerations.Flow;

/**
 * Message necessary to bind a Lane to a Semaphore. Message sent by City Actor to Semaphore Actor.
 *
 * @author Alberto Mulazzani
 */
public final class BindLaneMessage {

    private ActorRef lane;
    private Direction direction;
    private Flow flow;

    /**
     * @param lane      the Lane that should be bound to the receiving Semaphore
     * @param direction the Direction that the lane represent in the receiving Semaphore
     * @param flow      the flow of the Lane regarding the receiving Semaphore.
     */
    public BindLaneMessage(ActorRef lane, Direction direction, Flow flow) {
        this.lane = lane;
        this.direction = direction;
        this.flow = flow;
    }

    public ActorRef getLane() {
        return lane;
    }

    public Direction getDirection() {
        return direction;
    }

    public Flow getFlow() {
        return flow;
    }
}
