package messages.utility;

/**
 * Message from ComparisonViewer to a new City that sets the flag representing the construction
 * of non-Upuaut semaphores. This message is therefore used by the City when creating new Semaphores
 * to specify that they will have only the default behaviour.
 *
 * @author Michele Campobasso
 */
public final class IgnoreTrafficConditionsMessage {
}
