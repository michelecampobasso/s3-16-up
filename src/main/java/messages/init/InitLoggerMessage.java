package messages.init;

import utilities.enumerations.LoggerPriority;

/**
 * Message sent from the City Actor to initialize the Logger Actor
 *
 * @author Alberto Mulazzani
 */
public final class InitLoggerMessage {

    private LoggerPriority priorityToPrint;

    /**
     * @param priorityToPrint Minimum LoggerPriority that should be printed by the Logger Actor
     */
    public InitLoggerMessage(LoggerPriority priorityToPrint) {
        this.priorityToPrint = priorityToPrint;
    }

    public LoggerPriority getPriorityToPrint() {
        return priorityToPrint;
    }
}
