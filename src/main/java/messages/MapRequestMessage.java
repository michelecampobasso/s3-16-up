package messages;

/**
 * Message sent from a Vehicle Actor to the City Actor to request the CityMap.
 *
 * @author Alberto Mulazzani
 */
public final class MapRequestMessage {
}
