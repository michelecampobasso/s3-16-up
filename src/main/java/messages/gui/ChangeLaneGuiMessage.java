package messages.gui;

import akka.actor.ActorRef;
import utilities.LaneStatus;

/**
 * Message sent from the Lane Actor to the MapViewer to change the Lane status in the GUI
 *
 * @author Enrico Cagnazzo
 * @author Alessio Addimando
 */
public final class ChangeLaneGuiMessage {

    private ActorRef lane;
    private LaneStatus status;

    /**
     * @param lane   Lane whose status has changed
     * @param status the new status of the Lane
     */
    public ChangeLaneGuiMessage(ActorRef lane, LaneStatus status) {
        this.lane = lane;
        this.status = status;
    }

    public ActorRef getLane() {
        return lane;
    }

    public LaneStatus getStatus() {
        return status;
    }
}
