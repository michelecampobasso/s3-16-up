package entities.utility

import java.util

import entities.AbstractActorWithLogger
import gui.StatisticianResultDialog
import messages.bind.BindLoggerMessage
import messages.utility.{LaneTrafficInformationMessage, RequestStatisticsFileMessage}
import org.jfree.data.xy.{XYSeries, XYSeriesCollection}
import utilities.LaneIdentifier
import utilities.enumerations.LoggerPriority
import utilities.exceptions.StatisticsXmlFileCreationException
import utilities.filecreator.StatisticsXmlTextFileCreator

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

/**
  * Statistician Actor. It receives messages from all the other Lanes in the System
  * and tracks the status of the system computing useful statistics
  *
  * @author Alessio Addimando
  */
class Statistician extends AbstractActorWithLogger {

    private val fileWriter: StatisticsXmlTextFileCreator = new StatisticsXmlTextFileCreator
    private val lanes: mutable.HashMap[LaneIdentifier, ListBuffer[Double]] =
        new mutable.HashMap[LaneIdentifier, ListBuffer[Double]]()
    private val lanesStatistics: util.HashMap[LaneIdentifier, java.lang.Double] =
        new util.HashMap[LaneIdentifier, java.lang.Double]()
    private val plottingMapDataSet: mutable.HashMap[Double, Int] = new mutable.HashMap[Double, Int]()

    /**
      * The Statistician actor can receive and handle these messages:<br>
      *
      * LaneTrafficInformationMessage<br>
      * BindLoggerMessage<br>
      * RequestStatisticsFileMessage<br>
      */
    override def receive = {

        case message: LaneTrafficInformationMessage =>
            updateLaneInformation(message.getLaneIdentifier, message.getCurrentTrafficPercentage)

        case message: BindLoggerMessage => loggerRef = message.getLoggerRef

        case _: RequestStatisticsFileMessage =>
            updateStatistics()
            try {
                val newFileName = fileWriter writeToFile lanesStatistics
                createStatisticianView(newFileName)
                printLog("The statistics' file was successfully created!", LoggerPriority.HIGH)
            } catch {
                case _: StatisticsXmlFileCreationException =>
                    printLog("An error occurred during the creation of the file.", LoggerPriority.HIGH)
            }
    }

    /**
      * Updates the information about the lanes useful for statistics
      *
      * @param laneIdentifier    the identifier of the Lane
      * @param trafficPercentage the traffic percentage
      */
    private def updateLaneInformation(laneIdentifier: LaneIdentifier, trafficPercentage: Double): Unit = {

        if (!lanes.contains(laneIdentifier)) {
            lanes.put(laneIdentifier, new ListBuffer[Double]())
            lanes(laneIdentifier).+=(Math.round(trafficPercentage * 10) / 10.0)
        } else {
            lanes(laneIdentifier).+=(Math.round(trafficPercentage * 10) / 10.0)
        }
    }

    /**
      * Updates statistical data
      */
    private def updateStatistics(): Unit = {
        lanesStatistics.clear()
        for (currentLane <- lanes) {
            val averageValue: Double = Math.round(computeAverageValue(currentLane._2) * 10) / 10.0
            lanesStatistics.put(currentLane._1, averageValue)
            if (plottingMapDataSet.contains(averageValue))
                plottingMapDataSet(averageValue) = plottingMapDataSet(averageValue) + 1
            else plottingMapDataSet.put(averageValue, 1)
        }
    }

    /**
      * Simple function to compute computeAverageValue value of a numeric set
      *
      * @param setOfTrafficPercentage list with traffic percentages for each lane
      * @return the average value
      */
    private def computeAverageValue(setOfTrafficPercentage: ListBuffer[Double]): java.lang.Double =
        setOfTrafficPercentage.sum / setOfTrafficPercentage.length

    /**
      * Creates a view were is written the name of the statics file and able to create a plot
      *
      * @param newFileName The full name of the file created
      */
    private def createStatisticianView(newFileName: String): Unit =
        new StatisticianResultDialog(newFileName, convertStatisticMapToDatasetForPlotting())

    /**
      * Converts the statistics map in a XYSeries collection to plot dataset in a dedicated chart
      */
    private def convertStatisticMapToDatasetForPlotting(): XYSeriesCollection = {
        val series = new XYSeries("Average Traffic Statistics")
        for (currentEntry <- plottingMapDataSet) {
            series.add(currentEntry._1, currentEntry._2)
        }
        val dataSet = new XYSeriesCollection(series)
        dataSet
    }


}

