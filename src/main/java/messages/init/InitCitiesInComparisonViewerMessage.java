package messages.init;

import akka.actor.ActorRef;

/**
 * Message sent to bind the Cities to ComparisonViewer
 *
 * @author Michele Campobasso
 */
public final class InitCitiesInComparisonViewerMessage {
    private ActorRef upuautCityActor;
    private ActorRef standardCityActor;

    /**
     * @param upuautCityActor   the upuaut City Actor that has to be sent to Comparison Viewer
     * @param standardCityActor the standard City Actor that has to be sent to Comparison Viewer
     */
    public InitCitiesInComparisonViewerMessage(ActorRef upuautCityActor, ActorRef standardCityActor) {
        this.upuautCityActor = upuautCityActor;
        this.standardCityActor = standardCityActor;
    }

    public ActorRef getUpuautCityActor() {
        return upuautCityActor;
    }

    public ActorRef getStandardCityActor() {
        return standardCityActor;
    }
}
