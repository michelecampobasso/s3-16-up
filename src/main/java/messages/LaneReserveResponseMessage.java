package messages;

import utilities.enumerations.LaneReserveResponse;

/**
 * Message sent from a Lane Actor to a Vehicle Actor that returns his status.
 *
 * @author Alberto Mulazzani
 * @author Chiara Ceccarini
 */
public final class LaneReserveResponseMessage {
    private LaneReserveResponse status;

    /**
     * @param status LaneReserveResponse that represent if the Lane is Free or Full.
     */
    public LaneReserveResponseMessage(LaneReserveResponse status) {
        this.status = status;
    }

    public LaneReserveResponse getStatus() {
        return status;
    }
}
