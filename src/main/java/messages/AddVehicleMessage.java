package messages;

/**
 * Message sent from the GUI to the MapViewer, which forwarded it to the City Actor
 *
 * @author Enrico Cagnazzo
 */
public final class AddVehicleMessage {
    private int vehiclesNumber = 1;

    public AddVehicleMessage() {
    }

    /**
     * @param vehiclesNumber the number of vehicle to be added in the City
     */
    public AddVehicleMessage(int vehiclesNumber) {
        this.vehiclesNumber = vehiclesNumber;
    }

    public int getVehiclesNumber() {
        return vehiclesNumber;
    }
}
