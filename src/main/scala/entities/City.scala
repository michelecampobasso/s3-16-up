package entities

import java.util

import akka.actor.{ActorRef, Props}
import entities.dummyTestUtility.VehicleDispenser
import entities.utility.{Logger, Statistician}
import messages._
import messages.bind._
import messages.init.{InitCityMessage, InitLaneMessage, InitLoggerMessage, InitVehicleDispenserMessage}
import messages.utility.{AverageTravelTimeMessage, IgnoreTrafficConditionsMessage, StartMessage}
import utilities._
import utilities.enumerations.{Direction, Flow, LoggerPriority}

/**
  * City Actor. It initializes the Lane Actors, Semaphore Actors, VehicleDispenser Actor and Logger Actor.
  *
  * @author Alberto Mulazzani
  * @author Enrico Cagnazzo
  * @author Michele Campobasso
  *
  */
class City extends AbstractActorWithUpuautUtils {

    private val LANE_CAPACITY = 25
    private var nSemaphores = 0
    private var nVehicles = 0
    private var nVehiclesRunning = 0

    private var isStandardCity: Boolean = false
    private var standardCity: ActorRef = _

    private val vehicleDispenser: ActorRef = context.actorOf(Props[VehicleDispenser], "VehicleDispenser")

    private val statistician: ActorRef = context.actorOf(Props[Statistician], "Statistician")
    private val semaphores: util.ArrayList[util.ArrayList[ActorRef]] =
        new util.ArrayList[util.ArrayList[ActorRef]]()

    private val lanes: util.ArrayList[util.ArrayList[util.Map[Direction, ActorRef]]] =
        new util.ArrayList[util.ArrayList[util.Map[Direction, ActorRef]]]()

    private var cityMap: CityMap = _

    private var totalTravelTimes: Int = 0
    loggerRef = context.actorOf(Props[Logger])
    loggerRef ! new InitLoggerMessage(LoggerPriority.HIGH)
    vehicleDispenser ! new BindLoggerMessage(loggerRef)
    statistician ! new BindLoggerMessage(loggerRef)


    /**
      * The City Actor can accept and handle these messages:<br>
      *
      * IgnoreTrafficConditionsMessage<br>
      * InitCityMessage<br>
      * StartMessage<br>
      * CreateFirstsVehiclesMessage<br>
      * MapRequestMessage<br>
      * DestinationReachedMessage<br>
      * AddVehicleMessage<br>
      * VehicleStartAndDestinationMessage<br>
      * AddVehicleWithPositionMessage<br>
      */
    override def receive = {

        case _: IgnoreTrafficConditionsMessage =>
            isStandardCity = true

        case message: InitCityMessage =>
            nVehicles = message.getNumberOfVehicles
            nSemaphores = message.getNumberOfSemaphores
            viewer = message.getViewer
            createSemaphores()
            createLanes()
            cityMap = calculateMap()
            initView(lanes, semaphores, statistician, nVehicles)

        case _: StartMessage =>
            vehicleDispenser ! new InitVehicleDispenserMessage(lanes)

        case _: CreateFirstsVehiclesMessage =>
            vehicleDispenser ! new AddVehicleMessage(nVehicles)
            nVehiclesRunning = nVehicles

        case message: BindStandardCityMessage =>
            standardCity = message.getStandardCity

        case _: MapRequestMessage =>
            printLog("MapRequestMessage received", LoggerPriority.LOW)
            getContext().sender() ! new MapResponseMessage(cityMap)

        case message: DestinationReachedMessage =>
            nVehiclesRunning = nVehiclesRunning - 1
            changeNumberOfVehiclesOnGui(nVehiclesRunning)
            changeNumberOfArrivedVehiclesOnGui(isStandardCity)
            totalTravelTimes += message.getTravelTime
            if (nVehiclesRunning == 0) {
                if (isStandardCity) printLog("All the vehicles have reached their own destination in standard city and " +
                    "average travel time was " + totalTravelTimes / nVehicles, LoggerPriority.HIGH)
                else printLog("All the vehicles have reached their own destination in Upuaut city and " +
                    "average travel time was " + totalTravelTimes / nVehicles, LoggerPriority.HIGH)
                viewer ! new AverageTravelTimeMessage(isStandardCity, totalTravelTimes / nVehicles)
            }

        case message: AddVehicleMessage =>
            nVehiclesRunning = nVehiclesRunning + message.getVehiclesNumber
            changeNumberOfVehiclesOnGui(nVehiclesRunning)
            vehicleDispenser forward message

        case message: VehicleStartAndDestinationMessage =>
            if (standardCity != null)
                standardCity ! new AddVehicleWithPositionMessage(message.getStartVehiclePosition,
                    message.getDestinationVehiclePosition)

        case message: AddVehicleWithPositionMessage =>
            nVehiclesRunning = nVehiclesRunning + 1
            changeNumberOfVehiclesOnGui(nVehiclesRunning)
            vehicleDispenser forward message
    }

    /**
      * Method that begins the creation of Semaphores
      */
    private def createSemaphores(): Unit = {
        for (i <- 0 until nSemaphores) {
            val row: util.ArrayList[ActorRef] = new util.ArrayList[ActorRef]()
            for (j <- 0 until nSemaphores) {
                val sem: ActorRef = context.actorOf(Props[Semaphore], "Semaphore" + i + "-" + j)
                if (isStandardCity) sem ! new IgnoreTrafficConditionsMessage
                sem ! new BindLoggerMessage(loggerRef)
                sem ! new BindGuiActorMessage(viewer)
                sem ! new BindStatisticianMessage(statistician)
                printLog(sem.path.name + " created", LoggerPriority.LOW)
                row.add(sem)
            }
            semaphores.add(row)
        }
    }

    /**
      * Method that begins the creation of Lanes.
      * It puts the Lane inside the street.
      */
    private def createLanes(): Unit = {
        for (i <- 0 until nSemaphores) {
            val row: util.ArrayList[util.Map[Direction, ActorRef]] =
                new util.ArrayList[util.Map[Direction, ActorRef]]()
            for (j <- 0 until nSemaphores) {
                val street: util.Map[Direction, ActorRef] = new util.HashMap[Direction, ActorRef]
                if (i > 0)
                    street.put(Direction.NORTH, createLane(Direction.NORTH, (i, j), (i - 1, j), LANE_CAPACITY))
                if (j < nSemaphores - 1)
                    street.put(Direction.EAST, createLane(Direction.EAST, (i, j), (i, j + 1), LANE_CAPACITY))
                if (i < nSemaphores - 1)
                    street.put(Direction.SOUTH, createLane(Direction.SOUTH, (i, j), (i + 1, j), LANE_CAPACITY))
                if (j > 0)
                    street.put(Direction.WEST, createLane(Direction.WEST, (i, j), (i, j - 1), LANE_CAPACITY))
                row.add(street)
            }
            lanes.add(row)
        }
    }

    /**
      * Method that is called to create a lane.
      * It instantiate an ActorRef for the created Lane Actor and binds the Lane Actor as well as the two adjacent
      * Semaphore Actors.
      *
      * START -- new Lane --> END
      *
      * @param direction is the Direction that the lane will occupy in the Start Semaphore
      * @param start     is the tuple of the coordinates of the Start Semaphore Actor
      * @param end       is the tuple of the coordinates of the End Semaphore Actor
      * @param capacity  is the capacity of the Lane Actor
      * @return the Lane Actor ActorRef
      */
    private def createLane(direction: Direction, start: (Int, Int), end: (Int, Int), capacity: Int): ActorRef = {
        val lane: ActorRef =
            context.actorOf(Props[Lane], "Lane:" + start._1 + "-" + start._2 + "to" + end._1 + "-" + end._2)
        lane ! new InitLaneMessage(semaphores.get(start._1).get(start._2), semaphores.get(end._1).get(end._2),
            LANE_CAPACITY)
        lane ! new BindLoggerMessage(loggerRef)
        lane ! new BindGuiActorMessage(viewer)
        printLog(lane.path.name + " created", LoggerPriority.LOW)
        lane ! new BindStatisticianMessage(statistician)
        semaphores.get(start._1).get(start._2) ! new BindLaneMessage(lane, direction, Flow.OUTGOING)
        semaphores.get(end._1).get(end._2) !
            new BindLaneMessage(lane, Direction.oppositeDirection(direction), Flow.INCOMING)
        semaphores.get(start._1).get(start._2) ! new BindSemaphoreMessage(semaphores.get(end._1).get(end._2))
        semaphores.get(end._1).get(end._2) ! new BindSemaphoreMessage(semaphores.get(start._1).get(start._2))
        lane
    }

    /**
      * Method that is called to create the map for the vehicles.
      * For each lane there will be a Map of Direction->Lane with the adjacent lanes that the vehicle could take
      *
      * @return the CityMap
      */
    private def calculateMap(): CityMap = {
        val cityMapTmp = new CityMap()
        for (i <- 0 until lanes.size()) {
            for (j <- 0 until lanes.get(i).size()) {
                if (lanes.get(i).get(j).containsKey(Direction.NORTH))
                    cityMapTmp.addLane(lanes.get(i).get(j).get(Direction.NORTH),
                        new NextPossibleLanes(lanes.get(i - 1).get(j), Direction.NORTH))
                if (lanes.get(i).get(j).containsKey(Direction.EAST))
                    cityMapTmp.addLane(lanes.get(i).get(j).get(Direction.EAST),
                        new NextPossibleLanes(lanes.get(i).get(j + 1), Direction.EAST))
                if (lanes.get(i).get(j).containsKey(Direction.SOUTH))
                    cityMapTmp.addLane(lanes.get(i).get(j).get(Direction.SOUTH),
                        new NextPossibleLanes(lanes.get(i + 1).get(j), Direction.SOUTH))
                if (lanes.get(i).get(j).containsKey(Direction.WEST))
                    cityMapTmp.addLane(lanes.get(i).get(j).get(Direction.WEST),
                        new NextPossibleLanes(lanes.get(i).get(j - 1), Direction.WEST))
            }
        }
        cityMapTmp
    }
}