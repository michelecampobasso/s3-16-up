package messages.gui;

import akka.actor.ActorRef;
import utilities.enumerations.SemaphoreDirection;

/**
 * Message sent from the Semaphore Actor to the MapViewer to notify that the Semaphore's lights have changed
 *
 * @author Enrico Cagnazzo
 */
public final class ChangeSemaphoreLightsGuiMessage {

    private ActorRef semaphore;
    private SemaphoreDirection greenDirection;

    /**
     * @param semaphore      the Semaphore whose lights have changed
     * @param greenDirection the Direction of the green light
     */
    public ChangeSemaphoreLightsGuiMessage(ActorRef semaphore, SemaphoreDirection greenDirection) {
        this.semaphore = semaphore;
        this.greenDirection = greenDirection;
    }

    public ActorRef getSemaphore() {
        return semaphore;
    }

    public SemaphoreDirection getGreenDirection() {
        return greenDirection;
    }
}
