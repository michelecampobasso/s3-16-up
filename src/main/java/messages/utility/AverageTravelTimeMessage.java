package messages.utility;

/**
 * Message sent from the City to the ComparisonViewer to inform about the average travel time
 * of Vehicles in a City.
 *
 * @author Michele Campobasso
 */
public final class AverageTravelTimeMessage {

    private int averageTime;
    private boolean isStandardCity;

    /**
     * @param isStandardCity determines if this time belongs to the Standard City or Upuaut City
     * @param averageTime    the average travel time in seconds
     */
    public AverageTravelTimeMessage(boolean isStandardCity, int averageTime) {
        this.isStandardCity = isStandardCity;
        this.averageTime = averageTime;
    }

    public int getAverageTime() {
        return averageTime;
    }

    public boolean isStandardCity() {
        return isStandardCity;
    }

}
