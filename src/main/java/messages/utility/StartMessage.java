package messages.utility;

/**
 * Message sent from the GUI to the MapViewer, that forward it to the City
 *
 * @author Enrico Cagnazzo
 */
public final class StartMessage {
}
