package messages.utility;

/**
 * Message sent from the GUI to the Mapviewer, that forwarded it to the Statistician Actor to request statistics
 *
 * @author Alessio Addimando
 */
public final class RequestStatisticsFileMessage {
}
