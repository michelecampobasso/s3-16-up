package messages.gui;

import akka.actor.ActorRef;
import utilities.enumerations.SemaphoreStrategy;

/**
 * Message sent from the Semaphore to the MapView to update the GUI with the new strategy
 *
 * @author Enrico Cagnazzo
 */
public final class ChangeSemaphoreStrategyGuiMessage {
    private ActorRef semaphore;
    private SemaphoreStrategy strategy;

    /**
     * @param semaphore Semaphore whose strategy has changed
     * @param strategy  the new strategy of the Semaphore
     */
    public ChangeSemaphoreStrategyGuiMessage(ActorRef semaphore, SemaphoreStrategy strategy) {
        this.semaphore = semaphore;
        this.strategy = strategy;
    }

    public ActorRef getSemaphore() {
        return semaphore;
    }

    public SemaphoreStrategy getStrategy() {
        return strategy;
    }
}
