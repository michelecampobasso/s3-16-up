import akka.actor.{ActorRef, ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import entities.utility.{Logger, MapViewer}
import entities.{City, Lane, Semaphore}
import messages._
import messages.bind.{BindLaneMessage, BindLoggerMessage, BindSemaphoreMessage}
import messages.init.{InitLaneMessage, InitLoggerMessage}
import messages.utility.StartMessage
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import utilities.SemaphoreStatus
import utilities.enumerations.{Direction, Flow, LoggerPriority}

/**
  * @author Alberto Mulazzani
  * @author Michele Campobasso
  */
class SemaphoreTest extends TestKit(ActorSystem("MySpec")) with ImplicitSender
    with WordSpecLike with Matchers with BeforeAndAfterAll {

    val cityActor: ActorRef = system.actorOf(Props[City])
    var semaphoreActor: ActorRef = _
    var semaphoreActorCompanion: ActorRef = _
    val lane: ActorRef = system.actorOf(Props[Lane])
    val logger: ActorRef = system.actorOf(Props[Logger])
    val mapViewer: ActorRef = system.actorOf(Props[MapViewer])

    override def afterAll {
        TestKit.shutdownActorSystem(system)
    }

    override def beforeAll(): Unit = {
        // NOTE: Don't split the creation of semaphoreActor and logger binding!
        // Semaphore wants to change its status in 5 secs and needs logger.
        semaphoreActor = system.actorOf(Props[Semaphore])
        semaphoreActorCompanion = system.actorOf(Props[Semaphore])
        logger ! new InitLoggerMessage(LoggerPriority.LOW)
        semaphoreActor ! new BindLoggerMessage(logger)
        semaphoreActorCompanion ! new BindLoggerMessage(logger)
        lane ! new InitLaneMessage(semaphoreActor, semaphoreActorCompanion, 10)
        cityActor ! new StartMessage()
        logger ! new InitLoggerMessage(LoggerPriority.LOW)
        lane ! new BindLoggerMessage(logger)

    }

    "A Semaphore actor" must {

        "not reply to lane binding" in {
            semaphoreActor ! new BindLaneMessage(lane, Direction.WEST, Flow.OUTGOING)
            expectNoMsg()
        }

        "not reply to semaphore binding " in {
            semaphoreActor ! new BindSemaphoreMessage(semaphoreActorCompanion)
            expectNoMsg()
            semaphoreActorCompanion ! new BindSemaphoreMessage(semaphoreActor)
            expectNoMsg()
        }

        "reply to TrafficDetailsRequestMessage with a TrafficDetailsResponseMessage" in {
            semaphoreActor ! new TrafficDetailsRequestMessage()
            val reply = expectMsgClass(classOf[TrafficDetailsResponseMessage].getSuperclass)
            val semaphoreStatus: SemaphoreStatus = reply.asInstanceOf[TrafficDetailsResponseMessage].getSemaphoreStatus
            assert(semaphoreStatus.asInstanceOf[SemaphoreStatus].getIncomingLanes.isEmpty)
            assert(semaphoreStatus.asInstanceOf[SemaphoreStatus].getOutgoingLanes.isEmpty)
        }

        "send to self scheduled messages for changing status" in {
            semaphoreActor ! new ChangeStatusMessage()
            expectNoMsg()
        }
    }
}
