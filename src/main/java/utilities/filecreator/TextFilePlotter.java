package utilities.filecreator;

/**
 * Represents a generic text file plotter
 *
 * @author Alessio Addimando
 */
interface TextFilePlotter<W, V> {

    /**
     * plots the dataset starting from an origin text file
     *
     * @param datasetToPlot the dataSet to plot
     * @param originFile    the origin file
     */
    void plotDatasetFromFile(W datasetToPlot, V originFile);

    /**
     * processes the text document in order to plot it
     */
    void processTextDocument();

}
