package messages.utility;

import utilities.enumerations.LoggerPriority;

/**
 * Message sent from every Actor to the Logger Actor to print a Message.
 *
 * @author Alberto Mulazzani
 */
public final class PrintLogMessage {

    private String sender;
    private String message;
    private LoggerPriority priority;

    /**
     * @param sender   the Sender's name
     * @param message  the message that should be printed.
     * @param priority the LoggerPriority of the message.
     */
    public PrintLogMessage(String sender, String message, LoggerPriority priority) {
        this.sender = sender;
        this.message = message;
        this.priority = priority;
    }


    public String getMessage() {
        return "[" + sender + " -- " + priority + "] " + message;
    }

    public LoggerPriority getPriority() {
        return priority;
    }
}
