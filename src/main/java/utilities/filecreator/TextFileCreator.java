package utilities.filecreator;

import utilities.exceptions.StatisticsXmlFileCreationException;

import java.util.Map;

/**
 * Represents a generic text file creator
 *
 * @author Alessio Addimando
 */
interface TextFileCreator<X, Y, Z> {

    /**
     * Create a textFile starting from a generic map
     *
     * @param mapToWrite
     * @return The file created
     * @throws StatisticsXmlFileCreationException thrown when there's an error in the creation of the file
     */
    Z writeToFile(Map<X, Y> mapToWrite) throws StatisticsXmlFileCreationException;
}
