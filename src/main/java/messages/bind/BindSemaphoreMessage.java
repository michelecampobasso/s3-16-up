package messages.bind;

import akka.actor.ActorRef;

/**
 * Message to bind two Semaphore Actors. Sent by the City Actor to the Semaphore Actors
 *
 * @author Alberto Mulazzani
 */
public final class BindSemaphoreMessage {

    private ActorRef semaphore;

    /**
     * @param semaphore the other Semaphore Actor ActorRef
     */
    public BindSemaphoreMessage(ActorRef semaphore) {
        this.semaphore = semaphore;
    }

    public ActorRef getSemaphore() {
        return semaphore;
    }
}
